<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

Route::any('/admin/products', ['uses' => 'PaddleController@getAllPackages']);
Route::any('/admin/subscription/plans', ['uses' => 'PaddleController@getListSubscription']);
// Route::any('/admin/subscription/plans', ['uses' => 'PaddleController@getListSubscription']);
// Route::any('/admin/subscription/plans', ['uses' => 'PaddleController@getListSubscription']);
// Route::any('/admin/subscription/plans', ['uses' => 'PaddleController@getListSubscription']);
Route::any('/admin/subscription/users_cancel', ['uses' => 'PaddleController@cancelSubscription']);

Route::any('/hook/paddle', ['uses' => 'PaddleController@hookProcess']);


Route::get('/{any}', function () {
    return view('index');
})->where('any', '.*');
