server {
    listen 80;
	server_name app.habitify.me;
    index index.php index.html;
    root /var/www/public;
    location / {
        try_files $uri /index.php?$args;
    }
    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass app:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
}

server {
    listen 443 ssl;
	server_name app.habitify.me www.app.habitify.me;

	# config main
	index index.php index.html;
	root /var/www/public;
	
	location / {
		try_files $uri /index.php?$args;
	}

	location ~ \.php$ {
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass app:9000;
		fastcgi_index index.php;
		include fastcgi_params;
		fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
		fastcgi_param PATH_INFO $fastcgi_path_info;
	}

	# config ssl
	ssl_protocols TLSv1.2;
	ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA;
	ssl_prefer_server_ciphers on;
	ssl_ecdh_curve secp384r1:prime256v1;

	ssl_certificate /etc/letsencrypt/live/app.habitify.me/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/app.habitify.me/privkey.pem;

	ssl_session_tickets off;
	ssl_session_timeout 1d;
	ssl_session_cache shared:SSL:1m;

	# turn on gzip
	gzip on;
	gzip_comp_level 5;
	gzip_static on;
	gzip_buffers 4 8k;
	gzip_min_length 1100;
	gzip_vary on;
	gzip_proxied any;
	gzip_disable "MSIE [1-6]\.";
	gzip_types text/plain text/css application/javascript text/xml application/xml+rss application/json text/javascript application/x-javascript image/svg+xml;

	add_header Strict-Transport-Security 'max-age=63072000';

	# set expire date
	location ~* \.(js|css|png|jpg|jpeg|gif|ico|wmv|3gp|avi|mpg|mpeg|mp4|flv|mp3|mid|wml|swf|pdf|doc|docx|ppt|pptx|zip|svg|xml|woff|eot|ttf|otf)$ {
		expires 1M;
		access_log off;
		add_header Cache-Control public,immutable;
      	add_header Access-Control-Allow-Origin *;
	}
}
