require('./bootstrap');

import Vue from 'vue'
import MainApp from '@/MainApp'
import router from './router'
import store from './store'
import eventbus from './utils/eventBus'
import * as filters from './utils/filters'
import {sync} from 'vuex-router-sync'
import moment from 'moment-timezone'
import storage from 'store2'
import VueI18n from 'vue-i18n'
import VueTextareaAutosize from '@/plugins/vue-textarea-autosize/src'

import firebaseClient from './utils/firebaseSdk'
import paddleClient from './utils/paddleClient'

let lang = storage.get('locale', 'en')

var urlParam = new URLSearchParams(window.location.search)
if (urlParam.get('lang') && (urlParam.get('lang') === 'vi' || urlParam.get('lang') === 'en')) {
    lang = urlParam.get('lang')
    storage.set('locale', lang)
}

Vue.config.productionTip = false

sync(store, router)
let guess = moment.tz.guess()

moment.tz.setDefault(guess)
moment.locale(lang)

Vue.prototype.moment = moment
window.moment = moment
Vue.use(VueI18n)
Vue.use(eventbus)
// Vue.use(firebaseClient)
Vue.use(firebaseClient)
Vue.use(paddleClient)
Vue.use(VueTextareaAutosize)

Object.keys(filters).map((method) => {
    Vue.filter(method, filters[method])
})

const messages = {
    en: require('./i18n/en.json'),
    vi: require('./i18n/vi.json')
}

const i18n = new VueI18n({
    locale: lang,
    fallbackLocale: 'en',
    messages
})

/* eslint-disable no-new */
const app = new Vue({
    store,
    router,
    i18n,
    render: h => h(MainApp)
}).$mount('#app')

export default app