(function ($) {
    $.fn.weekdayChart = function (options) {
        // Target element which should detect swipes.
        var weekdayChart = this;
        var defaultSettings = {
            list: '',
            item: '.rate-item',
            wrapperChart: '.rate-circle',
            scaleItem: 'img',
            attrValue: 'data-value',
            attrStatus: 'data-status',
            optionMask: '.mask-weekday',
            imgDone: '/assets/images/circle-weekday-done.svg',
            imgSkip: '/assets/images/circle-weekday-skip.svg',
            imgFail: '/assets/images/circle-weekday-fail.svg'
        };
        var _options;

        // Initializer
        (function init() {
            _options = $.extend(defaultSettings, options);

            $(window).on('resize', initChart)
            initChart()
        })();

        function initChart() {
            setStatus()
            let wrapperSize = $(_options.wrapperChart).width()
                wrapperSize = wrapperSize > 35 ? 35 : wrapperSize
            $(_options.wrapperChart).css({
                height: wrapperSize + 'px'
            })

            setItemSize()
        }

        function setItemSize() {
            //max width: 40px
            let wrapperSize = 35//$(_options.wrapperChart).width()
            let itemList = $(_options.item)

            itemList.each((key, item) => {
                let objectScale = $(item).find(_options.scaleItem)
                let scaleData = $(objectScale).attr(_options.attrValue)
                
                let size = (parseFloat(scaleData)/100) * wrapperSize
                $(objectScale).css({
                    width: size + 'px',
                    height: size + 'px',
                    transition: 'all 200ms ease 0ms'
                })
            })
        }

        function setStatus() {
            let status = parseInt(weekdayChart.attr(_options.attrStatus))
            if (!status)
                status = parseInt($(_options.optionMask).attr(_options.attrStatus))
            
            let replaceObj = _options.item + ' ' + _options.scaleItem
            let img = _options.imgDone;
            if (status == 1)
                img = _options.imgSkip;
            if (status == 3)
                img = _options.imgFail;

            $(replaceObj).attr('src', img)
        }

        return weekdayChart; // Return element available for chaining.
    }

}(jQuery));