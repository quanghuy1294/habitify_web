; (function ($) {
    var pluginName = 'swipeSlide';

    function Plugin(element, options) {
        var el = element;
        var $el = $(element);

        // States: 0 - no swipe, 1 - swipe started, 2 - swipe released
        var swipeState = 0;
        // Coordinates when swipe started
        var startX = 0;
        var startY = 0;
        // Distance of swipe
        var pixelOffsetX = 0;
        var pixelOffsetY = 0;
        // Target element which should detect swipes.
        var swipeTarget = $el;
        var defaultSettings = {
            // Amount of pixels, when swipe don't count.
            swipeThreshold: 70,
            // Flag that indicates that plugin should react only on touch events.
            // Not on mouse events too.
            useOnlyTouch: false,
            swipeContainer: '.swiper-container',
            changeItemSize: null,
            containerItemSize: null,
            itemPerRow: 0,
            itemMargin: 1
        };
        var _options;

        var _scrollerWidth = 0;
        var _itemWidth = 0;
        var _itemCount = 0;
        var _itemActive = 0;

        

        _options = $.extend({}, defaultSettings, options);

        function init() {
            //_options = $.extend(defaultSettings, options);

            $(window).on('resize', () => {
                // resizeSlide()
                setTimeout(resizeSlide, 400)
            })
            initSlide()

            swipeTarget.off('mousedown touchstart').on('mousedown touchstart', swipeStart);
            $('html').off('mouseup touchend').on('mouseup touchend', swipeEnd);
            $('html').off('mousemove touchmove').on('mousemove touchmove', swiping);

            hook('onInit');
        }

        function option(key, val) {
            if (val) {
                _options[key] = val;
            } else {
                return _options[key];
            }
        }

        function destroy() {
            $el.each(function () {
                var el = this;
                var $el = $(this);

                // Add code to restore the element to its original state...

                hook('onDestroy');
                $el.removeData('plugin_' + pluginName);
            });
        }

        function hook(hookName) {
            if (_options[hookName] !== undefined) {
                _options[hookName].call(el);
            }
        }

        function initSlide() {
            _itemWidth = swipeTarget.width()
            _itemCount = $(_options.item).length
            _scrollerWidth = _itemCount * _itemWidth

            if (_options.activeIndex === 'last') {
                _itemActive = _itemCount - 1
            } else {
                _itemActive = _options.activeIndex
            }

            $(_options.item).css({ width: _itemWidth + 'px' });
            $(_options.polite).css({
                width: _scrollerWidth + 'px',
                transform: 'translate3d(0px, 0px, 0px)'
            })

            setPositionVerticle()
        }

        function resizeSlide() {
            _itemWidth = swipeTarget.width()
            _itemHeight = swipeTarget.height()
            _itemCount = $(_options.item).length
            _scrollerWidth = _itemCount * _itemWidth

            $(_options.item).css({ width: _itemWidth + 'px' });
            $(_options.polite).css({
                width: _scrollerWidth + 'px',
            })
            setPositionVerticle()
        }

        function swipeStart(event) {
            if (_options.useOnlyTouch && !event.originalEvent.touches)
                return;

            if (event.originalEvent.touches)
                event = event.originalEvent.touches[0];

            if (swipeState === 0) {
                swipeState = 1;
                startX = event.clientX;
                startY = event.clientY;
            }
        }

        function swipeEnd(event) {
            if (swipeState === 2) {
                swipeState = 0;

                if (Math.abs(pixelOffsetX) > Math.abs(pixelOffsetY) &&
                    Math.abs(pixelOffsetX) > _options.swipeThreshold) { // Horizontal Swipe
                    if (pixelOffsetX < 0) {
                        // if (_itemActive >= _itemCount - 1) {
                        //     return;
                        // }
                        // _itemActive++
                        // setPositionVerticle()
                        // swipeTarget.trigger($.Event('swipeLeft.sd'));
                    } else {
                        // if (_itemActive <= 0) {
                        //     return;
                        // }

                        // _itemActive--
                        // setPositionVerticle()
                        // swipeTarget.trigger($.Event('swipeRight.sd'));
                    }
                } else if (Math.abs(pixelOffsetY) > _options.swipeThreshold) { // Vertical swipe
                    if (pixelOffsetY < 0) {
                        swipeTarget.trigger($.Event('swipeUp.sd'));
                    } else {
                        swipeTarget.trigger($.Event('swipeDown.sd'));
                    }
                }
            }
        }

        function swiping(event) {
            // If swipe don't occuring, do nothing.
            if (swipeState !== 1)
                return;


            if (event.originalEvent.touches) {
                event = event.originalEvent.touches[0];
            }

            var swipeOffsetX = event.clientX - startX;
            var swipeOffsetY = event.clientY - startY;

            if ((Math.abs(swipeOffsetX) > _options.swipeThreshold) ||
                (Math.abs(swipeOffsetY) > _options.swipeThreshold)) {
                swipeState = 2;
                pixelOffsetX = swipeOffsetX;
                pixelOffsetY = swipeOffsetY;
            }
        }

        function setPositionVerticle() {
            let position = _itemActive * _itemWidth
            if (_itemCount <= 1) {
                position = 0
            }

            _itemHeight = swipeTarget.find(_options.item).eq(_itemActive).height()

            $(_options.polite).css({
                transform: 'translate3d(' + (-position) + 'px, 0px, 0px)',
                transition: 'transform 500ms ease 0ms'
            })
            setContainerHeight()
        }

        function setContainerHeight() {
            changeItemSize()

            _itemHeight = swipeTarget.find(_options.item).eq(_itemActive).height()
            $(_options.swipeContainer).css({
                height: _itemHeight + 'px',
                transition: 'height 500ms ease 0ms'
            })
        }

        function changeItemSize() {
            if (!_options.changeItemSize)
                return;
            var _containerWidth = $(_options.containerItemSize)
            let itemMargin = _options.itemMargin * _options.itemPerRow
            var _size = (_containerWidth.width() - itemMargin) / _options.itemPerRow

            $(_options.changeItemSize).css({
                width: _size + 'px',
                height: _size + 'px',
                'margin-right': _options.itemMargin
            })
        }

        function setActiveItem (index) {
            _itemActive = index
            setPositionVerticle()
        }

        init();

        return {
            option: option,
            destroy: destroy,
            setActiveItem: setActiveItem
        };
    }

    $.fn[pluginName] = function (options) {
        if (typeof arguments[0] === 'string') {
            var methodName = arguments[0];
            var args = Array.prototype.slice.call(arguments, 1);
            var returnVal;
            this.each(function () {
                if ($.data(this, 'plugin_' + pluginName) && typeof $.data(this, 'plugin_' + pluginName)[methodName] === 'function') {
                    returnVal = $.data(this, 'plugin_' + pluginName)[methodName].apply(this, args);
                } else {
                    throw new Error('Method ' + methodName + ' does not exist on jQuery.' + pluginName);
                }
            });
            if (returnVal !== undefined) {
                return returnVal;
            } else {
                return this;
            }
        } else if (typeof options === "object" || !options) {
            return this.each(function () {
                if (!$.data(this, 'plugin_' + pluginName)) {
                    $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
                }
            });
        }
    };

    $.fn[pluginName].defaults = {
        onInit: function () { },
        onDestroy: function () { }
    };

})(jQuery);