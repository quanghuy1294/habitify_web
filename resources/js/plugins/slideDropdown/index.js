;( function( $, window, undefined ) {

	'use strict';

	// global
	var $body = $( 'body' );

	$.SDLMenu = function( options, element ) {
		this.$el = $( element );
		this._init( options );
	};

	// the options
	$.SDLMenu.defaults = {
		// classes for the animation effects
        animationClasses : { classin : 'dl-animate-in-1', classout : 'dl-animate-out-1' },
        toggleClass: 'show-calendar',
        ignoreHide: ['.habitify-datepicker'],
		// callback: click a link that has a sub menu
		// el is the link element (li); name is the level name
		onLevelClick : function( el, name ) { return false; },
		// callback: click a link that does not have a sub menu
		// el is the link element (li); ev is the event obj
		onLinkClick : function( el, ev ) { return false; },
		backLabel: 'Back',
		// Change to "true" to use the active item as back link label.
		useActiveItemAsBackLabel: false,
		// Change to "true" to add a navigable link to the active item to its child
		// menu.
		useActiveItemAsLink: false,
		// On close reset the menu to root
        resetOnClose: true,
        initIndexItem: 0,
        slideInIndexItem: 1,
        maxHeightIndex: 1,
		setHeight: true,
		normalScroll: true,
		backdrop: false
	};

	$.SDLMenu.prototype = {
		_init : function( options ) {

			// options
			this.options = $.extend( true, {}, $.SDLMenu.defaults, options );

			// cache some elements and initialize some variables
			this._config();

			var animEndEventNames = {
					'WebkitAnimation' : 'webkitAnimationEnd',
					'OAnimation' : 'oAnimationEnd',
					'msAnimation' : 'MSAnimationEnd',
					'animation' : 'animationend'
				},
				transEndEventNames = {
					'WebkitTransition' : 'webkitTransitionEnd',
					'MozTransition' : 'transitionend',
					'OTransition' : 'oTransitionEnd',
					'msTransition' : 'MSTransitionEnd',
					'transition' : 'transitionend'
				};
			// animation end event name
			this.animEndEventName = animEndEventNames[ 'animation' ] + '.dlmenu';
			// transition end event name
			this.transEndEventName = transEndEventNames[ 'transition' ] + '.dlmenu';
			// support for css animations and css transitions
			this.supportAnimations = true;
			this.supportTransitions = true;

			this._initEvents();
		},
		_config : function() {
			this.open = false;
			this.$trigger = this.$el.children( '.sld-trigger' );
            this.$menu = this.$el.children( '.sdl-menu-content' );
            this.$menuwrapper = this.$menu.find('.wrapper');
            this.$pickers = this.$menu.find('.block-picker');
            this.$menuitems = this.$menu.find( 'li:not(.sdl-back)' );
            this.$menucustom = this.$menu.find( 'li.sdl-toggle' );
            this.$back = this.$menu.find( '.sdl-back' );
            
            // set first height
            this._setHeightMenu()
            
		},
		_initEvents : function() {
            var self = this;

            $(window).off('resize.sdlmenu').on('resize.sdlmenu', function() {
                self._closeMenu()
            })

			this.$trigger.off('click.sdlmenu').on( 'click.sdlmenu', function() {
				if( self.open ) {
					self._closeMenu();
				}
				else {
					self._openMenu();
					// clicking somewhere else makes the menu close
					$body.off( 'click' ).children().off( 'click.sdlmenu').on( 'click.sdlmenu', function(e) {
                        let ignore = false
                        //'.habitify-datepicker'
                        self.options.ignoreHide.forEach(item => {
                            let isCalendar = $(e.target).closest(item)
                            if (isCalendar.length)
                                ignore = true
                        })

                        if (ignore)
                            return false
                        
						self._closeMenu() ;
					} );

				}
				return false;
            } );
            
            this.$menucustom.on( 'click.sdlmenu', function( event ) {
                event.stopPropagation();
				self.$menuwrapper.toggleClass(self.options.toggleClass)

				let hasShowCalendar = self.$menuwrapper.hasClass(self.options.toggleClass)

                let height = $(self.$pickers[self.options.initIndexItem]).attr('data-height') + 'px'
                if (hasShowCalendar) {
                    height = $(self.$pickers[self.options.slideInIndexItem]).attr('data-height') + 'px'
                }

                let maxHeight = $(self.$pickers[self.options.maxHeightIndex]).attr('data-height') + 'px'
                
                if (hasShowCalendar) {
                    self.$menu.scrollTop(0)
                    self.$menu.css({
						height: height,
                        overflow: 'hidden',
                        overflowY: 'hidden'
                    })
                } else {
                    let normalCss = {
                        height: height,
                        overflow: 'hidden',
                        overflowY: 'hidden'
					}
					
					if (self.options.normalScroll) {
						normalCss['overflow-y'] = 'auto'
						normalCss['maxHeight'] = maxHeight
					}

					if (self.options.backdrop && window.innerWidth < 1024) {
						normalCss['maxHeight'] = '100vh'
						normalCss['height'] = '100vh'
					}

					self.$menu.css(normalCss)
                }
			} );
		},
		closeMenu : function() {
			if( this.open ) {
				this._closeMenu();
			}
		},
		_closeMenu : function() {
			var self = this,
				onTransitionEndFn = function() {
					self.$menu.off( self.transEndEventName );
					if( self.options.resetOnClose ){
						self._resetMenu();
					}
				};

			this.$menu.removeClass( 'sdl-menuopen' );
			this.$menu.addClass( 'sdl-menu-toggle' );
			this.$trigger.removeClass( 'sdl-active' );

			if( this.supportTransitions ) {
				this.$menu.on( this.transEndEventName, onTransitionEndFn );
			}
			else {
				onTransitionEndFn.call();
			}

			this.open = false;
		},
		openMenu : function() {
			if( !this.open ) {
				this._openMenu();
			}
		},
		_openMenu : function() {
			var self = this;
			// clicking somewhere else makes the menu close
			// $body.off( 'click' ).on( 'click.sdlmenu', function(e) {
			// 	self._closeMenu() ;
            // } );
            $('.sdl-menu').sdlmenu('closeMenu')
            this._setHeightMenu()
			this.$menu.addClass( 'sdl-menuopen sdl-menu-toggle' ).on( this.transEndEventName, function() {
				$( this ).removeClass( 'sdl-menu-toggle' );
			} );
			this.$trigger.addClass( 'sdl-active' );
			this.open = true;
		},
		// resets the menu to its original state (first level of options)
		_resetMenu : function() {
			this.$menuwrapper.removeClass(this.options.toggleClass)
        },
        _setHeightMenu: function() {
            var self = this
            if (!self.options.setHeight) {
                return false
            }

            self.$menu.css({height: 'auto'})

            setTimeout(() => {
                $.each(self.$pickers, function(index, item) {
                    let height = $(item).find('.block-picker-wrapper').height()
                    $(item).attr('data-height', height)
                })

                let height = $(self.$pickers[self.options.initIndexItem]).attr('data-height') + 'px'
                if (self.$menuwrapper.hasClass(self.options.toggleClass)) {
                    height = $(self.$pickers[self.options.slideInIndexItem]).attr('data-height') + 'px'
                }

                let maxHeight = $(self.$pickers[self.options.maxHeightIndex]).attr('data-height') + 'px'
                
                if (self.$menuwrapper.hasClass(self.options.toggleClass)) {
                    self.$menu.css({
						height: height,
						maxHeight: 'auto',
                        overflow: 'hidden',
                        'overflow-y': 'hidden'
                    })
                } else {
					let normalCss = {
                        height: height,
                        overflow: 'hidden',
					}

					if (self.options.normalScroll) {
						normalCss['overflow-y'] = 'auto'
						normalCss['maxHeight'] = maxHeight
					}

					if (self.options.backdrop && window.innerWidth < 1024) {
						normalCss['maxHeight'] = '100vh'
						normalCss['height'] = '100vh'
					}

                    self.$menu.css(normalCss)
                }
            }, 0)
        }
	};

	var logError = function( message ) {
		if ( window.console ) {
			window.console.error( message );
		}
	};

	$.fn.sdlmenu = function( options ) {
		if ( typeof options === 'string' ) {
			var args = Array.prototype.slice.call( arguments, 1 );
			this.each(function() {
				var instance = $.data( this, 'sdlmenu' );
				if ( !instance ) {
					logError( "cannot call methods on dlmenu prior to initialization; " +
					"attempted to call method '" + options + "'" );
					return;
				}
				if ( !$.isFunction( instance[options] ) || options.charAt(0) === "_" ) {
					logError( "no such method '" + options + "' for sdlmenu instance" );
					return;
				}
				instance[ options ].apply( instance, args );
			});
		}
		else {
			this.each(function() {
				var instance = $.data( this, 'sdlmenu' );
				if ( instance ) {
					instance._init(options);
				}
				else {
					instance = $.data( this, 'sdlmenu', new $.SDLMenu( options, this ) );
				}
			});
		}
		return this;
	};

} )( jQuery, window );