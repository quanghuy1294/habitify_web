(function ($) {
    $.fn.streakChart = function (options) {
        // Target element which should detect swipes.
        var streakChart = this;
        var defaultSettings = {
            list: '.rate-list',
            item: '.streak-item',
            label: '.streak-label',
            streakWidth: '.streak-width',
            widthAttr: 'data-length',
            maxAttr: 'data-max',
            chart: '.streak-list'
        };
        var _options;

        // Initializer
        (function init() {
            _options = $.extend(defaultSettings, options);

            $(window).on('resize', initChart)
            initChart()
        })();

        function initChart() {
            calculateMaxSize()
            setTimeout(function () {
                calculateMaxSize()
            }, 1000)
        }

        function calculateMaxSize() {
            let itemWidth = $(_options.item).width()
            let labelWidth = $(_options.label).width()
            let maxLabelWidth = 0
            $(_options.label).each((index, item) => {
                maxLabelWidth = maxLabelWidth < $(item).width() ? $(item).width() : maxLabelWidth
            })

            let items = $(_options.item)
            let maxStreakLength = 0
            items.each((key, item) => {
                let streak = $(item).find(_options.streakWidth)
                let width = parseInt(streak.attr(_options.widthAttr))
                maxStreakLength = width > maxStreakLength ? width : maxStreakLength
            })
            
            let maxWidth = itemWidth - (maxLabelWidth*2) - 28
            items.each((key, item) => {
                let streak = $(item).find(_options.streakWidth)
                let width = parseInt(streak.attr(_options.widthAttr))
                let streakSize = (width/maxStreakLength) * maxWidth
                streak.css({
                    width: streakSize + 'px',
                    transition: 'all 200ms ease 0ms'
                })
            })

            let labelFixWidth = maxLabelWidth <= 0 ? 'auto' : maxLabelWidth + 'px'
            $(_options.label).css({width: labelFixWidth})
            
        }

        return streakChart; // Return element available for chaining.
    }

}(jQuery));