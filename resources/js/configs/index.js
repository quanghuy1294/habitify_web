export const apiUrl = process.env.API_URL || 'http://localhost'

export const languages = {
    'vi': 'VN',
    'en': 'EN'
}

export const firebaseConfigs = {
    apiKey: "AIzaSyDOW_-yQsdDOUGypwJ4Sk2XVaE4oEWhwUM",
    authDomain: "habitify-development.firebaseapp.com",
    databaseURL: "https://habitify-development.firebaseio.com",
    projectId: "habitify-development",
    storageBucket: "habitify-development.appspot.com",
    messagingSenderId: "401972781117"
};

export const firebaseConfigsProd = {
    apiKey: "AIzaSyBQICSt0dmOxeR3AePW1vKKm5yi1MGNhEQ",
    authDomain: "habitify.firebaseapp.com",
    databaseURL: "https://habitify.firebaseio.com",
    projectId: "project-8491986773398252429",
    storageBucket: "project-8491986773398252429.appspot.com",
    messagingSenderId: "51819855588",
    appId: "1:51819855588:web:1b774380f3198151"
};

export const firebasePublicMessageKey = {
    dev: 'BA9MKgm9MMAy_qcl0fCjl7BL2YbCZaOUwU7os09wldj1_OZvDiSL5dgw02sB-SfyFDUlQv_gAgP3wCEmhdxXl2A',
    prod: 'BNw47fgdW5w41Bn_6C2OJxjWi-qe_Zr0fJDhyqh4dpt9TJe5J4TozBr7Xk-XzLI4v32Cy2-ywtDpvy-897hPjEc'
}

export const momentConfigDate = 'DDMMYYYY'
export const momentDDMMYYYY = 'DDMMYYYY'
export const momentDD_MM_YYYY = 'DD-MM-YYYY'
export const dateConfigFormat = {
    note: 'YYYY-MM-DDTHH:mm:ss',
    DDMMYYYY: 'DDMMYYYY',
    FORMAT_EXPIRED_DATE: "YYYY-MM-DDTHH:mm:ssZ",
    DIFF_YEAR: 'MMMM DD, YYYY',
    SAME_YEAR: 'dddd, MMMM DD',
    STREAK_CHART: 'MMM DD',
    META_DATA: "YYYY-MM-DDTHH:mm:ssZ",
    NOTE_FORMAT: "dddd, D MMMM YYYY HH:mm"
}

export const HABIT_STATUS_FAIL = 3
export const HABIT_STATUS_NONE = 0
export const HABIT_STATUS_SKIP = 1
export const HABIT_STATUS_DONE = 2

export const habitStatus = {
    none: 0,
    skip: 1,
    done: 2,
    fail: 3
}

export const timeOfDay = {
    morning: 1,
    afternoon: 2,
    evening: 4,
    night: 4,
    anyTime: 7
}

export const timeOfDayPath = {
    'allHabits': timeOfDay.anyTime,
    'morning': timeOfDay.morning,
    'afternoon': timeOfDay.afternoon,
    'evening': timeOfDay.evening,
    'night': timeOfDay.night,
}

export const labelTimeOfDayPath = {
    7: 'allHabits',
    1: 'morning',
    4: 'evening',
    2: 'afternoon',
}

export const regularly = {
    daily: 'daily',
    weekly: 'weekly',
    weekdays: 'weekDays',
    interval: 'dayInterval'
}

export const firstWeekDay = {
    startSunday: 1,
    startMonday: 2
}

export const labelTimeOfDays = {
    1: 'common.morning',
    2: 'common.afternoon',
    4: 'common.evening',
    7: 'common.all.habit'
}

export const labelTimeDropdownOfDays = {
    1: 'common.morning',
    2: 'common.afternoon',
    4: 'common.evening',
    7: 'common.anytime'
}

export const eventNames = {
    toggleUpgradePremium: 'toggleUpgradePremium',
    toggleDialogEdit: 'toggleDialogEdit',
    toggleNotesHabit: 'toggleNotesHabit',
    changeUserPremiumData: 'changeUserPremiumData',
    changeTabHabitDetail: 'changeTabHabitDetail',
    updateSingleHabit: 'updateSingleHabit',

    
    fetchUserData: 'fetchUserData',
    singleHabitUpdateRegularly: 'singleHabitUpdateRegularly',
    singleHabitUpdateArchive: 'singleHabitUpdateArchive',
    singleHabitUpdateName: 'singleHabitUpdateName',
    singleHabitUpdatePriority: 'singleHabitUpdatePriority',
    singleHabitUpdateStartDate: 'singleHabitUpdateStartDate',
    singleHabitUpdateTimeOfDay: 'singleHabitUpdateTimeOfDay',
    singleHabitUpdateCheckins: 'singleHabitUpdateCheckins',
    singleHabitUpdateReminders: 'singleHabitUpdateReminders',

    habitDetailReDrawCalendar: 'habitDetailReDrawCalendar',
    habitDetailChangeDisplayDate: 'habitDetailChangeDisplayDate',
    habitDetailSetCalendarMonth: 'habitDetailSetCalendarMonth',
    habitDetailSetListMonth: 'habitDetailSetListMonth',
    habitDetailSetMonth: 'habitDetailSetMonth',
    habitDetailSetYear: 'habitDetailSetYear',
    habitDetailCloseEditDialog: 'habitDetailCloseEditDialog',

    managerUpdateHabit: 'managerUpdateHabit',
    managerSelectHabit: 'managerSelectHabit',
    managerResetData: 'managerResetData',

    commonForceClosePopup: 'commonForceClosePopup',
    journalForceClosePopup: 'journalForceClosePopup',
    habitDetailForceClosePopup: 'habitDetailForceClosePopup',
    managerHabitForceClosePopup: 'managerHabitForceClosePopup',

    journalCloseEditDialog: 'journalCloseEditDialog',
    journalShowCreateDialog: 'journalShowCreateDialog',

    mainAppToggleHabit: 'mainAppToggleHabit',
    mainAppSetHabitStatus: 'mainAppSetHabitStatus',
    mainAppSetArchiveHabit: 'mainAppSetArchiveHabit',
    mainAppUpdatePageStatus: 'mainAppUpdatePageStatus',
    mainAppChangeHabitList: 'mainAppChangeHabitList',
    mainAppChangeHabit: 'mainAppChangeHabit',
    mainAppAddNewHabit: 'mainAppAddNewHabit',
    mainAppSetSelectedDate: 'mainAppSetSelectedDate',
    mainAppForceCloseDropdown: 'mainAppForceCloseDropdown',

    authRemoveNotifyToken: 'authRemoveNotifyToken',

    progressForceClosePopup: 'progressForceClosePopup',
    progressShowCreateDialog: 'progressShowCreateDialog',
    progressSetHabitDetail: 'progressSetHabitDetail',

    loaderIsLoaded: 'loaderIsLoaded',
    loaderIsLoading: 'loaderIsLoading',

    commonCloseDialogCreateHabit: 'commonCloseDialogCreateHabit',
    commonShowDialogCreateHabit: 'commonShowDialogCreateHabit',

    toggleTopbarDatePicker: 'toggleTopbarDatePicker',
    topbarToggleShowCompleted: 'topbarToggleShowCompleted',
    
    commonSignout: 'commonSignout',
    upgradePremiumSetPackage: 'upgradePremiumSetPackage'
}

export const deviceTypes = {
    mobile: 1,
    tablet: 2,
    pc: 3
}

export const breakpoints = {
    pc: 1024,
    tablet: 768,
    mobile: 0
}

export const paddleConfigs = {
    vendor_id: 48402,
    vendor_key: 'b8d25a4409b84da2d5f6974c4a2bd485c24536d65aa85c2d23'
}

export const customCountry = {
    us: 'US',
    ca: 'CA',
    ja: 'JP',
    ru: 'RU',
    de: 'DE',
    fr: 'FR',
    gb: 'GB',
    sg: 'SG',
    kr: 'KR',
    tr: 'TR',
    mx: 'MX',
    sv: 'SV',
    it: 'IT',
    zh: 'CN',
    es: 'ES',
    pt: 'PT',
}

export const currencyPrefix = {
    US: '$',
    CA: 'CA$',
    JP: '¥',
    RU: '₽',
    DE: '€',
    FR: '€',
    GB: '£',
    SG: '$',
    KR: '₩',
    TR: '‎₺',
    MX: '$',
    SV: 'kr',
    IT: '€',
    CN: '¥',
    ES: '€',
    PT: '€',
}

export const currencyPrefixISO = {
    US: 'USD',
    CA: 'CAD',
    JP: 'JPY',
    RU: 'RUB',
    DE: 'EUR',
    FR: 'EUR',
    GB: 'GBP',
    SG: 'SGD',
    KR: 'KRW',
    TR: '‎TRY',
    MX: 'MXN',
    SV: 'SEK',
    IT: 'EUR',
    CN: 'CNY',
    ES: 'EUR',
    PT: 'EUR',
}

export const premiumStatus = {
    androidFree: 0,
    androidPremium: 1,
    iosFree: 0,
    iosPremium: 9829
}

export const limitOfFreeUser = {
    habits: 3,
    darkmode: false,
    export: false,
    skip: false,
    archive: false,
    viewYear: false,
    notes: false,
    reminders: 3
}

export const paddleAlertNames = {
    payment_succeeded: 'payment_succeeded',
    payment_refunded: 'payment_refunded',
    subscription_payment_succeeded: 'subscription_payment_succeeded',
    subscription_payment_refunded: 'subscription_payment_refunded'
}

export const tooltipChart = {
    chartRateDetail: {
        enabled: false,
        mode: 'nearest',
        callbacks: {
        },
        custom: function(tooltipModel, data) {
            let labels = this._data.labels
            
            // Tooltip Element
            var tooltipEl = document.getElementById('chartjs-tooltip');

            // Create element on first render
            if (!tooltipEl) {
                tooltipEl = document.createElement('div');
                tooltipEl.id = 'chartjs-tooltip';
                tooltipEl.innerHTML = '<div class="tooltip-c-content"></div>'
                document.body.appendChild(tooltipEl);
            }

            // Hide if no tooltip
            if (tooltipModel.opacity === 0) {
                tooltipEl.style.opacity = 0;
                return;
            }

            // Set caret Position
            tooltipEl.classList.remove('above', 'below', 'no-transform');
            if (tooltipModel.yAlign) {
                tooltipEl.classList.add(tooltipModel.yAlign);
            } else {
                tooltipEl.classList.add('no-transform');
            }

            function getBody(bodyItem) {
                return bodyItem.lines;
            }

            // Set Text
            if (tooltipModel.body) {
                let _title = tooltipModel.title[0]
                let _body = tooltipModel.body.map(getBody)[0]
                    _body = Number.isInteger(_body) ? _body : parseFloat(_body).toFixed(1)
                let _indexTitle = labels.indexOf(_title)
                console.log('gen title: ', labels[_indexTitle - 1], _title, labels[_indexTitle + 1])
                let resultText = ''
                let newLabel = ''
                if (_title.indexOf('W') >= 0 || _title.indexOf('w') >= 0 ) {
                    if (_indexTitle >= labels.length-1)
                        newLabel = labels[_indexTitle - 1] ? `${labels[_indexTitle - 1]} - ${_title}` : _title
                    else
                        newLabel = labels[_indexTitle + 1] ? `${_title} - ${labels[_indexTitle + 1]}` : _title

                } else
                    newLabel = _title
                
                resultText += `<span class="tooltip-c-title">${newLabel}</span>`
                resultText += `<span class="tooltip-c-body">${_body}%</span>`
                
                tooltipEl.querySelector('.tooltip-c-content').innerHTML = resultText
            }

            var position = this._chart.canvas.getBoundingClientRect();
            let minPos = position.left
            let maxPos = position.left + position.width
            
            let activePoint = this._chart.tooltip._active[0]
            let x = activePoint.tooltipPosition().x
            let tWidth = tooltipEl.clientWidth > 200 ? 120 : tooltipEl.clientWidth
            let pos = minPos + (x - tWidth/2)
            
            if (pos < minPos) {
                pos = minPos
            } else if (pos >= maxPos || pos + tWidth >= maxPos) {
                pos = maxPos - tWidth
            }

            // Display, position, and set styles for font
            tooltipEl.style.opacity = 1;
            tooltipEl.style.position = 'absolute';
            tooltipEl.style.left = (pos) + 'px';
            tooltipEl.style.top = (position.top - tooltipEl.clientHeight) + 'px';
            tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
            tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
            tooltipEl.style.pointerEvents = 'none';
        }
    },

    chartCheckinTime: {
        enabled: false,
        mode: 'nearest',
        callbacks: {
        },
        custom: function(tooltipModel, data) {
            let labels = this._data.labels
            
            // Tooltip Element
            var tooltipEl = document.getElementById('chartjs-tooltip');

            // Create element on first render
            if (!tooltipEl) {
                tooltipEl = document.createElement('div');
                tooltipEl.id = 'chartjs-tooltip';
                tooltipEl.innerHTML = '<div class="tooltip-c-content"></div>'
                document.body.appendChild(tooltipEl);
            }

            // Hide if no tooltip
            if (tooltipModel.opacity === 0) {
                tooltipEl.style.opacity = 0;
                return;
            }

            // Set caret Position
            tooltipEl.classList.remove('above', 'below', 'no-transform');
            if (tooltipModel.yAlign) {
                tooltipEl.classList.add(tooltipModel.yAlign);
            } else {
                tooltipEl.classList.add('no-transform');
            }

            function getBody(bodyItem) {
                return bodyItem.lines;
            }

            // Set Text
            if (tooltipModel.body) {
                let _title = tooltipModel.title[0]
                let _body = tooltipModel.body.map(getBody)[0]
                let _indexTitle = labels.indexOf(_title)
                let resultText = ''
                if (_indexTitle >= labels.length-1) {
                    resultText += `<span class="tooltip-c-title">${labels[_indexTitle - 1]} - ${_title}</span>`
                } else {
                    resultText += `<span class="tooltip-c-title">${_title} - ${labels[_indexTitle + 1]}</span>`
                }
                resultText += `<span class="tooltip-c-body">${_body}</span>`
                
                tooltipEl.querySelector('.tooltip-c-content').innerHTML = resultText
            }

            var position = this._chart.canvas.getBoundingClientRect();
            let minPos = position.left
            let maxPos = position.left + position.width
            
            let activePoint = this._chart.tooltip._active[0]
            let x = activePoint.tooltipPosition().x
            let tWidth = tooltipEl.clientWidth > 200 ? 120 : tooltipEl.clientWidth
            let pos = minPos + (x - tWidth/2)

            if (pos < minPos) {
                pos = minPos
            } else if (pos >= maxPos || pos + tWidth >= maxPos) {
                pos = maxPos - tWidth
            }

            // Display, position, and set styles for font
            tooltipEl.style.opacity = 1;
            tooltipEl.style.position = 'absolute';
            tooltipEl.style.left = (pos) + 'px';
            tooltipEl.style.top = (position.top - tooltipEl.clientHeight) + 'px';
            tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
            tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
            tooltipEl.style.pointerEvents = 'none';
        }
    }
}

export const modelMetaData = {
    habitId: "h",
    targetDate: "td", // date checkin for
    status: "st",
    timestamp: "t", // time click checkin
    deviceId: "d",
    source: "s",
}

export const checkinSources = {
    journal: 0,
    singleProgress: 1,
    todayWidget: 2,
    notification: 3,
    notificationWidget: 4,
    siriShortcut: 5,
    xCallbackURL: 6,
    macOSMenuBar: 7,
}

export const upgradeKey = {
    habit: 'habit',
    skip: 'skip',
    archive: 'archive',
    yearly: 'yearly',
    notes: 'notes',
    reminder: 'reminder',
}

export const dataUpgrades = {
    habit: {
        icon: '/assets/images/premium/unlimited-habit.svg',
        title: 'newupgrade.benefits.unlimited.habit.title',
        content: 'newupgrade.benefits.unlimited.habit.description'
    },
    skip: {
        icon: '/assets/images/premium/upgrade-skip.svg',
        title: 'newupgrade.benefits.skip.title',
        content: 'newupgrade.benefits.skip.description'
    },
    archive: {
        icon: '/assets/images/premium/archive-habit.svg',
        title: 'newupgrade.benefits.archive.title',
        content: 'newupgrade.benefits.archive.description'
    },
    yearly: {
        icon: '/assets/images/premium/yearly-calendar.svg',
        title: 'newupgrade.benefits.yearly.calendar.title',
        content: 'newupgrade.benefits.yearly.calendar.description'
    },
    notes: {
        icon: '/assets/images/premium/notes.svg',
        title: 'newupgrade.benefits.notes.title',
        content: 'newupgrade.benefits.notes.description'
    },
    reminder: {
        icon: '/assets/images/premium/reminder.svg',
        title: 'newupgrade.benefits.unlimited.reminder.title',
        content: 'newupgrade.benefits.unlimited.reminder.description'
    },
}

export const preferences = {
    openNote: 'openNote',
    currentPage: 'currentPage',
    progress_habit_review: 'progress_habit_review',
    progress_daily_performance: 'progress_daily_performance',
    progress_checkin_time: 'progress_checkin_time',
    progress_completion_rate_type: 'progress_completion_rate_type',
    progress_completion_rate_num: 'progress_completion_rate_num',
    detail_completion_rate_type: 'detail_completion_rate_type',
    detail_completion_rate_num: 'detail_completion_rate_num',
    detail_weekday_status: 'detail_weekday_status',
    manager_habit_type: 'manager_habit_type',
    timeofday: 'timeofday',
    currentDate: 'currentDate',
    show_completed_habits: 'show_completed_habits'
}

export const profileActions = {
    profileSettings: 'profileSettings',
    upgradePremium: 'upgradePremium',
    signOut: 'signOut' 
}