import { 
    firebaseConfigs, habitStatus, regularly, firstWeekDay, dateConfigFormat, 
    customCountry, currencyPrefix, premiumStatus, timeOfDay
} from '@/configs'

import * as language from '@/plugins/vuejs-datepicker/dist/locale'
import storage from 'store2'

import app from '@/app'
import moment from 'moment'
import _ from 'lodash'

const getWeeklyCount = (_regularly) => {
    return _regularly.replace(regularly.weekly + '-', '')
}

const getWeekDaysCount = (_regularly) => {
    let str = _regularly.replace(regularly.weekdays + '-', '')
    let days = str.split(',')

    return days.length
}

const getRegularlyCountFromString = (_regularly, regex) => {
    return parseInt(_regularly.replace(regex + '-', ''))
}

/**
 * 
 * @param {Object} habit 
 * @param {Date} startDateOfWeek 
 */
const getTotalActualCheckInThisWeek = (habit, startDateOfWeek) => {
    let totalCheckIn = 0
    if (!habit || !habit.hasOwnProperty('checkins')) {
        return totalCheckIn
    }

    let checkins = habit.checkins
    for (let i = 0; i < 7; i++) {
        let dateOutput = moment(startDateOfWeek.toDate()).add(i, 'days').format('DDMMYYYY')
        let status = checkins[dateOutput]
        if (status != null && parseInt(status) === habitStatus.done) {
            totalCheckIn++;
        }
    }

    return totalCheckIn;
}

const isHabitDone = (habit, currentDate) => {
    let currentDateOutput = moment(currentDate.toDate()).format('DDMMYYYY')
    let checkins = habit.hasOwnProperty('checkins') ? Object.keys(habit.checkins) : []
    if (isOutOfRangeDate(habit, currentDate))
        return false

    return checkins.indexOf(currentDateOutput) >= 0 && habit.checkins[currentDateOutput] === habitStatus.done
}

const getHabitStatus = (habit, currentDate) => {
    let status = habitStatus.none
    let currentDateOutput = moment(currentDate.toDate()).format('DDMMYYYY')
    let checkins = habit.hasOwnProperty('checkins') ? Object.keys(habit.checkins) : []
    if (checkins.indexOf(currentDateOutput) < 0) {
        return status
    }

    return habit.checkins[currentDateOutput]
}

const isInFeature = (habit, currentDate) => {
    let _startDate = getStartTime(habit.startDate).startOf('day')
    let _currentDate = moment(currentDate.toDate()).startOf('day')

    return _startDate > _currentDate
}

const isOutOfRangeDate = (habit, date, includeTomorrow = false) => {
    let _startDate = getStartTime(habit.startDate).startOf('day')
    let _today = moment().startOf('day')
    let _tomorrow = moment(_today.toDate()).add(1, 'day')
    let _current = moment(date.toDate()).startOf('day')
    
    if (includeTomorrow)
        return _current > _tomorrow || _current < _startDate    

    return _current > _today || _current < _startDate
}

/**
 * 
 * @param {number} startDate : habit.startDate
 */
const getStartTime = (startDate) => {
    return moment(startDate * 1000, 'x').startOf('day')
}

const getIsArchived = (habit) => {
    return habit.isArchived ? true : false
}

const regularlyIsValid = (habit, currentDate) => {
    if (habit.regularly.indexOf(regularly.weekly) >= 0 || habit.regularly.indexOf(regularly.daily) >= 0) {
        return true;
    } else if (habit.regularly.indexOf(regularly.interval) >= 0) {
        let _habitStartDate = getStartTime(habit.startDate).startOf('day')
        let _currentDate = moment(currentDate.toDate()).startOf('day')
        let loopLength = parseInt(habit.regularly.replace(regularly.interval + '-', ''))
        let _diffDays = _currentDate.diff(_habitStartDate, 'day')

        return _diffDays % loopLength === 0
    }

    return false;
}

//
const getTriggerTimes = (timeTriggers) => {
    let results = []
    Object.keys(timeTriggers).forEach ((time) => {
        if (timeTriggers[time]) {
            let _currentFmts = time.split(':')
            _currentFmts.forEach((clue, index) => {
                if (parseInt(clue) < 10)
                    _currentFmts[index] = '0' + clue
            })
            
            results.push(_currentFmts.join(':'))
        }
    })

    let listHours = []
    results.forEach(hour => {
        listHours.push(parseInt(moment(hour, "HH:mm").format('X')))
    })
    listHours = listHours.sort()
    
    let listFormatSort = []
    listHours.forEach(hour => {
        listFormatSort.push(moment(hour, 'X').format('HH:mm'))
    })

    return listFormatSort
}

const getTriggerLeft = (_regularly, _habit, currentDate) => {
    let weeklyCount = getWeeklyCount(_regularly)
    let totalCheckInThisWeek = getTotalActualCheckInThisWeek(_habit, currentDate)

    return totalCheckInThisWeek < weeklyCount ? weeklyCount - totalCheckInThisWeek : 0
}

const dateIsFeatureOrPast = (startDate, compareDate) => {
    let maxDate = parseInt(moment().startOf('day').format('X'))
    let minDate = parseInt(getStartTime(startDate).startOf('day').format('X'))
    let _compareDate = parseInt(moment(compareDate.toDate()).startOf('day').format('X'))

    return _compareDate > maxDate || _compareDate < minDate
}

const getFirstDayOfWeek = (date, iso = false) => {
    let tmp = moment(date.toDate())
    return iso ? tmp.startOf('isoWeek') : tmp.startOf('week')
}

const getEndDayOfWeek = (date, iso = false) => {
    let tmp = moment(date.toDate())
    return iso ? tmp.endOf('isoWeek') : tmp.endOf('week')
}

const getWeekOfYear = (date, iso = false) => {
    let tmp = moment(date.toDate())
    return iso ? tmp.format('W') : tmp.format('w')
}

const getCurrentTime = (isUtc = false) => {
    if (isUtc)
        return moment.utc().format(dateConfigFormat.note)
    else
        return moment().format(dateConfigFormat.note)
}

const getCalendarRange = (habit) => {
    let today = moment()
    let maxDate = moment().add(1, 'days')
    let minDate = getStartTime(habit.startDate)
    return {
        to: minDate.toDate(), // Disable all dates up to specific date
        from: maxDate.toDate(), // Disable all dates after specific date
    }
}

const getListDropdown = (i18n, lang) => {
    moment.locale(lang)
    let _today = moment().startOf('day')
    let _tomorrow = moment().startOf('day').add(1, 'days')
    let _yesterday = moment().startOf('day').subtract(1, 'days')
    let _prev2day = moment().startOf('day').subtract(2, 'days')
    let _prev3day = moment().startOf('day').subtract(3, 'days')

    return [
        {
            label: i18n.t('common.tomorrow'),
            date: _tomorrow,
            seconds: parseInt(_tomorrow.format('X')),
            dateFormat: _tomorrow.format('MMM DD'),
        },
        {
            label: i18n.t('common.today'),
            date: _today,
            seconds: parseInt(_today.format('X')),
            dateFormat: _today.format('MMM DD')
        },
        {
            label: i18n.t('common.yesterday'),
            date: _yesterday,
            seconds: parseInt(_yesterday.format('X')),
            dateFormat: _yesterday.format('MMM DD')
        },
        // {
        //     label: _.capitalize(_prev2day.format('dddd')),
        //     date: _prev2day,
        //     seconds: parseInt(_prev2day.format('X')),
        //     dateFormat: _prev2day.format('MMM DD')
        // },
        // {
        //     label: _.capitalize(_prev3day.format('dddd')),
        //     date: _prev3day,
        //     seconds: parseInt(_prev3day.format('X')),
        //     dateFormat: _prev3day.format('MMM DD')
        // },
    ]
}

const getMomentWeekDay = (dow, lang, iso = true, type = 'full') => {
    let dayList = getMomentWeekDays(lang, iso, type)
    return _.capitalize(dayList[dow])
}

const getMomentWeekDays = (lang, iso = true, type = 'full') => {
    moment.locale(lang)
    let weekDays = []

    if(type === 'min')
        weekDays = moment.weekdaysMin()
    else if(type === 'short')
        weekDays = moment.weekdaysShort()
    else
        weekDays = moment.weekdays()

    if (iso) {
        weekDays.push(weekDays.shift())
    }

    return weekDays
}

const isEqualDateWithFormat = (date1, date2, format = dateConfigFormat.DDMMYYYY) => {
    let f1 = moment(date1.toDate()).format(format)
    let f2 = moment(date2.toDate()).format(format)

    return f1 === f2
}

const habitCheckedInDays = (habit, date, diffCurWeek) => {
    let checkin = 0
    for (let i = 0; i <= diffCurWeek; i++) {
        let tmp = moment(date.toDate()).add(i, 'days')
        
        let statusOfDay = getHabitStatus(habit, tmp)
        if (statusOfDay === habitStatus.skip)
            continue
        else if (statusOfDay !== habitStatus.done)
            break
        else if (statusOfDay === habitStatus.done)
            checkin++
    }

    return checkin
}

const habitConsecutiveDailyCheckin = (habit, currentDate) => {
    let _currentDate = moment(currentDate.toDate()).startOf('day')
    let today = moment().startOf('day')
    let _minDate = getStartTime(habit.startDate)
    let diffFromNow = moment.duration(_currentDate.diff(_minDate)).as('days')
        diffFromNow = _currentDate.format('DDMMYYYY') == _minDate.format('DDMMYYYY') ? 1 : diffFromNow 
    let count = 0
    for (let i = 0; i < diffFromNow; i++) {
        let tmp = moment(_currentDate.toDate()).subtract(i, 'days')
        let statusOfDay = getHabitStatus(habit, tmp)

        if (statusOfDay === habitStatus.skip)
            continue
        else if (statusOfDay !== habitStatus.done) {
            if (isEqualDateWithFormat(tmp, today))
                continue
            break
        } else if (statusOfDay === habitStatus.done)
            count++
    }

    return count
}

const habitConsecutiveWeekDaysCheckin = (habit, currentDate, iso) => {
    let _currentDate = moment(currentDate.toDate()).startOf('day')
    let _currentWeek = getWeekOfYear(_currentDate, iso)
    let _minDate = getStartTime(habit.startDate)
    let diffFromNow = moment.duration(_currentDate.diff(_minDate)).as('days')

    let count = 0
    for (let i = 0; i < diffFromNow; i++) {
        let tmp = moment(_currentDate.toDate()).locale('en').subtract(i, 'days')
        let _dayOfWeek = tmp.format('ddd').toLowerCase()
        let _weekOfYear = getWeekOfYear(tmp, iso)
        let statusOfDay = getHabitStatus(habit, tmp)

        if (habit.regularly.indexOf(_dayOfWeek) <= 0 || statusOfDay === habitStatus.skip)
            continue
        else if (statusOfDay !== habitStatus.done) {
            //continue if day in current week
            if (_weekOfYear === _currentWeek)
                continue
            break
        } else if (statusOfDay === habitStatus.done)
            count++
    }

    return count
}

const habitConsecutiveIntervalCheckin = (habit, currentDate, iso) => {
    let requireDates = generateRequiresInterval(habit)

    let count = 0
    for (let i = requireDates.length - 1; i >= 0; i--) {
        let tmp = requireDates[i]
        let statusOfDay = getHabitStatus(habit, requireDates[i])

        if (isOutOfRangeDate(habit, tmp))
            continue

        if (statusOfDay === habitStatus.skip)
            continue
        else if (statusOfDay !== habitStatus.done) {
            break
        } else if (statusOfDay === habitStatus.done)
            count++
    }

    return count
}

const habitConsecutiveWeeklyCheckin = (habit, currentDate, iso) => {
    let _currentDate = moment(currentDate.toDate())
    let _currentWeekOfYear = getWeekOfYear(_currentDate, iso)
    let _minDate = getStartTime(habit.startDate)


    let firstCurWeek = getFirstDayOfWeek(_currentDate, true)
    let diffCurWeek = moment.duration(_currentDate.diff(firstCurWeek)).as('days')
    
    let validCount = 0
    //let validCount = habitCheckedInDays(habit, firstCurWeek, diffCurWeek)

    let firstWeek = parseInt(moment(_minDate.toDate()).format('w'))
    let lastWeek = parseInt(moment(_currentDate.toDate()).format('w'))
    let totalWeek = (lastWeek - firstWeek) + 1
    // let totalWeek = moment.duration(firstCurWeek.diff(_minDate)).as('weeks')
    //     totalWeek = totalWeek > parseInt(totalWeek) ? parseInt(totalWeek + 1) : totalWeek

    let mustCheckin = getRegularlyCountFromString(habit.regularly, regularly.weekly)
    for (let _week = 0; _week < totalWeek; _week++) {
        let _firstDayOfWeek = getFirstDayOfWeek(moment(firstCurWeek.toDate()).subtract(_week, 'weeks'), iso)
        let _weekOfYear = getWeekOfYear(_firstDayOfWeek, iso)
        let skip = 0
        let done = 0
        for (let _day = 0; _day < 7; _day++) {
            let tmp = moment(_firstDayOfWeek.toDate()).add(_day, 'days')
            let status = getHabitStatus(habit, tmp)
            skip += status === habitStatus.skip ? 1 : 0
            done += status === habitStatus.done ? 1 : 0
        }

        let isGreatThanRequire = (skip + done) >= mustCheckin
        // if not great than require in current week of today then break
        // if (_weekOfYear === _currentWeekOfYear && !isGreatThanRequire)
        //     break

        if (_weekOfYear === _currentWeekOfYear) {
            validCount += done
        } else if ((skip + done) >= mustCheckin) {
            validCount += done
        } else { // break if week not ok with require checkin
            break
        }
    }

    return validCount
}

const habitGetExpectCheckin = (habit, currentDate) => {
    if (!habit || !Object.keys(habit).length) {
        return 0
    }

    let _regularly = habit.regularly
    let _currentDate = moment(currentDate.toDate()).startOf('day')
    let dayOfWeek = moment(currentDate.toDate()).locale('en').format('ddd').toLowerCase()
    let _expectDays = 0

    // if is daily
    if (_regularly === regularly.daily) {
        _expectDays = 7
    } else if (_regularly.indexOf(regularly.weekdays) >= 0) { // if weekDays
        _expectDays = getWeekDaysCount(_regularly)
    } else if (_regularly.indexOf(regularly.weekly) >= 0) { // if weekly
        _expectDays = getRegularlyCountFromString(_regularly, regularly.weekly)
    } else if (_regularly.indexOf(regularly.interval) >= 0) { // if interval
        let intervalTime = getRegularlyCountFromString(_regularly, regularly.interval)
        _expectDays = parseInt(7/intervalTime)
    }

    return _expectDays
}

const pIsStartWithMonday = (preferences) => {
    return parseInt(preferences.firstWeekDay) === firstWeekDay.startMonday
}

const getWeekDays = (iso = false) => {
    let _weedayStartWithSun = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"]
    let _weedayStartWithMon = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]

    let _weekDay = iso ? _weedayStartWithMon : _weedayStartWithSun

    return _weekDay
}

const streaksHabitDaily = (habit, currentDate) => {
    let _streakList = []
    let _currentDate = moment(currentDate.toDate()).startOf('day')
    let _minDate = getStartTime(habit.startDate)
    let diffDays = moment.duration(_currentDate.diff(_minDate)).as('days')
        diffDays = diffDays > parseInt(diffDays) ? parseInt(diffDays) + 1 : diffDays

    let maxStreak = 0
    let _startStreak = []
    let _maxDateStreak = []
                
    for (let _day = 0; _day <= diffDays; _day++) {
        let tmp = moment(_minDate.toDate()).add(_day, 'days')
        let statusOfDay = getHabitStatus(habit, tmp)
        
        maxStreak = _startStreak.length > maxStreak ? _startStreak.length : maxStreak
        if (statusOfDay === habitStatus.skip) {
            _maxDateStreak.push(tmp)
            continue
        } else if (statusOfDay !== habitStatus.done) {
            if (_startStreak.length > 1) {
                _streakList.push({
                    length: _startStreak.length,
                    start: _maxDateStreak[0],
                    end: _maxDateStreak[_maxDateStreak.length - 1],
                    milisecond: parseInt(moment(_startStreak[0].toDate()).format('x'))
                })
            }
            _startStreak = []
            _maxDateStreak = []
            continue
        } else if (statusOfDay === habitStatus.done) {
            _startStreak.push(tmp)
            _maxDateStreak.push(tmp)
            maxStreak = _startStreak.length > maxStreak ? _startStreak.length : maxStreak
        }
    }
    
    if (_startStreak.length > 1) {
        _streakList.push({
            length: _startStreak.length,
            start: _maxDateStreak[0],
            end: _maxDateStreak[_maxDateStreak.length - 1],
            milisecond: parseInt(moment(_startStreak[0].toDate()).format('x'))
        })
    }

    return streaksSort(_streakList, maxStreak)
}

const streaksHabitWeekdays = (habit, currentDate) => {
    let _streakList = []
    let _currentDate = moment(currentDate.toDate()).startOf('day')
    let _minDate = getStartTime(habit.startDate)
    let diffDays = moment.duration(_currentDate.diff(_minDate)).as('days')

    let maxStreak = 0
    let _startStreak = []
    let _maxDateStreak = []

    for (let _day = 0; _day <= diffDays; _day++) {
        let tmp = moment(_currentDate.toDate()).subtract(_day, 'days')
        let _dayOfWeek = tmp.format('ddd').toLowerCase()
        let statusOfDay = getHabitStatus(habit, tmp)

        maxStreak = _startStreak.length > maxStreak ? _startStreak.length : maxStreak
        if (habit.regularly.indexOf(_dayOfWeek) < 0) {
            continue
        }

        if (statusOfDay === habitStatus.skip) {
            _maxDateStreak.push(tmp)
            continue
        } else if (statusOfDay !== habitStatus.done) {
            if (_startStreak.length > 1) {
                _streakList.push({
                    length: _startStreak.length,
                    start: _maxDateStreak[_maxDateStreak.length - 1],
                    end: _maxDateStreak[0],
                    milisecond: parseInt(moment(_startStreak[0].toDate()).format('x'))
                })
            }
            
            _startStreak = []
            _maxDateStreak = []
            continue
        } else if (statusOfDay === habitStatus.done) {
            _startStreak.push(tmp)
            _maxDateStreak.push(tmp)
            maxStreak = _startStreak.length > maxStreak ? _startStreak.length : maxStreak
        }
    }

    // finish loop
    if (_startStreak.length > 1) {
        _streakList.push({
            length: _startStreak.length,
            start: _maxDateStreak[_maxDateStreak.length - 1],
            end: _maxDateStreak[0],
            milisecond: parseInt(moment(_startStreak[0].toDate()).format('x'))
        })
    }
    
    return streaksSort(_streakList, maxStreak)   
}

const streaksHabitDayInterval = (habit, currentDate) => {
    let _streakList = []
    let _currentDate = moment(currentDate.toDate()).startOf('day')
    let _minDate = getStartTime(habit.startDate)
    let totalDates = moment.duration(_currentDate.diff(_minDate)).as('days')
    let intervalTime = getRegularlyCountFromString(habit.regularly, regularly.interval)
    let diffDays = (totalDates/intervalTime + 1)

    let maxStreak = 0
    let _startStreak = []
    let _maxDateStreak = []

    for (let _interval = 0; _interval < diffDays; _interval++) {
        let tmp = moment(_minDate.toDate()).add((_interval * intervalTime), 'days')
        let statusOfDay = getHabitStatus(habit, tmp)

        maxStreak = _startStreak.length > maxStreak ? _startStreak.length : maxStreak
        
        if (statusOfDay === habitStatus.skip) {
            _maxDateStreak.push(tmp)
            continue
        } else if (statusOfDay !== habitStatus.done) {
            if (_startStreak.length > 1) {
                _streakList.push({
                    length: _startStreak.length,
                    start: _maxDateStreak[0],
                    end: _maxDateStreak[_maxDateStreak.length - 1],
                    milisecond: parseInt(moment(_startStreak[0].toDate()).format('x'))
                })
            }
            _startStreak = []
            _maxDateStreak = []
            continue
        } else if (statusOfDay === habitStatus.done) {
            _startStreak.push(tmp)
            _maxDateStreak.push(tmp)
            maxStreak = _startStreak.length > maxStreak ? _startStreak.length : maxStreak
            if (_startStreak.length > 1 && _interval >= (diffDays - 1)) {
                _streakList.push({
                    length: _startStreak.length,
                    start: _maxDateStreak[0],
                    end: _maxDateStreak[_maxDateStreak.length - 1],
                    milisecond: parseInt(moment(_startStreak[0].toDate()).format('x'))
                })
            }
        }
    }

    return streaksSort(_streakList, maxStreak)
    
}

const streaksHabitDayWeekly = (habit, currentDate, iso) => {
    let _currentDate = moment(currentDate.toDate()).startOf('day')
    let _minDate = getStartTime(habit.startDate)

    let firstCurWeek = getFirstDayOfWeek(_currentDate, iso)
    
    let totalWeek = moment.duration(firstCurWeek.diff(_minDate)).as('weeks')
        totalWeek = totalWeek > parseInt(totalWeek) ? totalWeek + 1 : totalWeek

    let mustCheckin = getRegularlyCountFromString(habit.regularly, regularly.weekly)
    let _streakList = []
    let maxStreak = 0;

    let _startStreak = []
    let _startSkipStreak = []
    let _maxDateStreak = []
    let breakStreak = false
    for (let _week = 0; _week < totalWeek; _week++) {
        let _firstDayOfWeek = getFirstDayOfWeek(moment(_minDate.toDate()).add(_week, 'weeks'), iso)
        let _weekSkip = 0
        let _weekDone = []
        let _weekSkipDay = []
        
        for (let i = 0; i <= 6; i++) {
            let tmp = moment(_firstDayOfWeek.toDate()).add(i, 'days')
            let statusOfDay = getHabitStatus(habit, tmp)
            
            if (statusOfDay === habitStatus.skip) {
                _weekSkip++
                _weekSkipDay.push(tmp)
                continue
            } else if (statusOfDay !== habitStatus.done) {
                continue
            } else if (statusOfDay === habitStatus.done) {
                _weekDone.push(tmp)
                _weekSkipDay.push(tmp)
            }
        }

        // Nếu tuần hiện tại bé hơn tổng số tuần và skip > 2 hoặc skip và done > 0 hoặc done > tổng phải checkin
        if (
            _week < parseInt(totalWeek) &&
            (
                _weekSkip >= 2 ||
                (_weekSkip > 0 && _weekDone.length > 0) ||
                _weekDone.length >= mustCheckin
            )
        ) {
            // streak continue calc
            _weekDone.forEach(item => {
                _startStreak.push(item)
            })

            _weekSkipDay.forEach(item => {
                _startSkipStreak.push(item)
            })

            

        } else {
            _weekDone.forEach(item => {
                _startStreak.push(item)
            })

            _weekSkipDay.forEach(item => {
                _startSkipStreak.push(item)
            })
            
            if (_startStreak.length >= mustCheckin) {
                _streakList.push({
                    length: _startStreak.length,
                    start: _startSkipStreak[0],
                    end: _startSkipStreak[_startSkipStreak.length - 1],
                    milisecond: parseInt(moment(_startStreak[0].toDate()).format('x'))
                })

                maxStreak = _startStreak.length > maxStreak ? _startStreak.length : maxStreak
            }
            _startStreak = []
            _startSkipStreak = []
        }
    }

    return streaksSort(_streakList, maxStreak)
}

const streaksSort = (_streakList, max) => {
    let sortByLength = _.orderBy(_streakList, ['length', 'milisecond'], ['desc', 'asc']).slice(0, 8)
    let sorted = _.orderBy(sortByLength, ['milisecond'], ['asc'])

    return {list: sorted, max: max}
}

const generateRequiresInterval = (habit) => {
    let _currentDate = moment().startOf('day')
    let _minDate = getStartTime(habit.startDate)

    let intervalTime = getRegularlyCountFromString(habit.regularly, regularly.interval)
    let _expectDays = parseInt(7/intervalTime)
    let totalDayFromNow = moment.duration(_currentDate.diff(_minDate)).as('days')

    let requireds = []
    for (let _interval = 0; _interval < (totalDayFromNow/intervalTime + 1); _interval++) {
        requireds.push(moment(_minDate.toDate()).add((_interval * intervalTime), 'days'))
    }
    return requireds
}

const getCompletionRateDailyOld = (habit, yearCalendar, iso = false) => {
    let today = moment().endOf('day')
    let minDate = getStartTime(habit.startDate)
    let maxDate = moment(today.toDate()).endOf('month')
    let diffDates = moment.duration(today.diff(minDate)).as('days')
    let currentWeek = getWeekOfYear(today, iso)

    let expectCheckinOfWeek = habitGetExpectCheckin(habit, moment())
    let labelWeek = []
    let dataWeek = []

    let labelMonth = []
    let dataMonth = []
    let tmpResult = {}

    let currentLang = storage.get('locale', 'en')

    // loop with year data input
    let yearKeys = Object.keys(yearCalendar)
    yearKeys.forEach(_year => {
        if (!tmpResult.hasOwnProperty(_year)) {
            tmpResult[_year] = {}
        }

        let monthKeys = Object.keys(yearCalendar[_year]).sort()
        monthKeys.forEach(_month => {

            // prepare group data week by week
            if (!tmpResult[_year].hasOwnProperty(_month)) {
                tmpResult[_year][_month] = {}
            }

            let days = yearCalendar[_year][_month].days
            days.forEach(dayItem => {
                let tmp = moment(dayItem.day.toDate()).startOf('day')
                let _weekOfYear = getWeekOfYear(tmp, iso)
                if (tmp > moment().startOf('day'))
                    return false

                if (!tmpResult[_year][_month].hasOwnProperty(_weekOfYear)) {
                    tmpResult[_year][_month][_weekOfYear] = 0
                }
                tmpResult[_year][_month][_weekOfYear] += dayItem.status
            })

            let _monthName = moment(yearCalendar[_year][_month].month.toDate()).format('MMMM').toLowerCase()
            _monthName = _monthName === 'january' ? _year : app.$i18n.t('calendar.short.month.'+_monthName)
            if (labelMonth[labelMonth.length - 1] !== _monthName) {
                labelMonth.push(_monthName)
            }

            // start calculate rate by week
            let monthData = tmpResult[_year][_month]
            let weekKeyData = Object.keys(monthData)

            let rateMonth = 0
            let weekKeys = []
            weekKeyData.forEach(week => {
                if (parseInt(week) > parseInt(currentWeek)) {
                    return false
                }
                weekKeys.push(week)
            })
            weekKeys.forEach(week => {
                let weekRates = (monthData[week]/expectCheckinOfWeek)*100

                // add week label
                let weekLabel = week <= 1 ? _year : 'W' + week
                if (labelWeek[labelWeek.length - 1] !== weekLabel) {
                    labelWeek.push(weekLabel)

                    dataWeek.push(weekRates)
                    rateMonth += weekRates
                }
            })

            dataMonth.push(rateMonth/weekKeys.length) 
        })
    })

    // for (let _day = 0; _day < diffDates; _day++) {
    //     let tmp = moment(minDate.toDate()).startOf('day').add(_day, 'days')
    //     let status = isHabitDone(habit, tmp)
    //     let _year = parseInt(tmp.format('YYYY'))
    //     let _month = parseInt(tmp.format('MM'))
    //     let _monthName = tmp.format('MMMM').toLowerCase()
    //     let _weekOfYear = getWeekOfYear(tmp, iso)
    //     let _increaseCount = status ? 1 : 0

    //     if (!tmpResult.hasOwnProperty(_year)) {
    //         tmpResult[_year] = {}
    //     }

    //     if (!tmpResult[_year].hasOwnProperty(_month)) {
    //         tmpResult[_year][_month] = {}
    //     }

    //     if (!tmpResult[_year][_month].hasOwnProperty(_weekOfYear)) {
    //         tmpResult[_year][_month][_weekOfYear] = 0
    //     }
    //     tmpResult[_year][_month][_weekOfYear] += _increaseCount

    //     _monthName = _monthName.toLowerCase() === 'january' ? _year : app.$i18n.t('calendar.short.month.'+_monthName)
    //     if (labelMonth[labelMonth.length - 1] !== _monthName) {
    //         labelMonth.push(_monthName)
    //     }
    // }

    // Loop
    // let yearKeys = Object.keys(tmpResult)
    // yearKeys.forEach(year => {
    //     let monthKeys = Object.keys(tmpResult[year])
    //     monthKeys.forEach(month => {
    //         let monthData = tmpResult[year][month]
    //         let weekKeys = Object.keys(monthData)

    //         let rateMonth = 0
    //         weekKeys.forEach(week => {
    //             let weekRates = (monthData[week]/expectCheckinOfWeek)*100

    //             // add week label
    //             let weekLabel = week <= 1 ? year : 'W' + week
    //             if (labelWeek[labelWeek.length - 1] !== weekLabel) {
    //                 labelWeek.push(weekLabel)

    //                 dataWeek.push(weekRates)
    //                 rateMonth += weekRates
    //             }
    //         })

    //         dataMonth.push(rateMonth/weekKeys.length) 
    //     })
    // })

    // rate current week
    let rateCurrentWeek = dataWeek[dataWeek.length - 1]
    let avgWeek = _.mean(dataWeek)

    // rate current month
    let startDayOfMonth = moment(today.toDate()).startOf('month')
    let diffToStartMonth = moment.duration(today.diff(startDayOfMonth)).as('days')
    let checkedIn = 0
    for (let _day = 0; _day < diffToStartMonth; _day++) {
        let _tmpDate = moment(startDayOfMonth.toDate()).add(_day, 'days')
        if (isHabitDone(habit, _tmpDate))
            checkedIn++
    }

    let rateCurrentMonth = (checkedIn/(diffToStartMonth*expectCheckinOfWeek)) * 100
    let avgMonth = _.mean(dataMonth)
    dataMonth[dataMonth.length - 1] = parseFloat(rateCurrentMonth).toFixed(1)

    let result = {
        dayDatas: tmpResult,

        dataWeek: dataWeek,
        labelWeek: labelWeek,

        labelMonth: labelMonth,
        dataMonth: dataMonth,

        rateCurrentWeek: rateCurrentWeek,
        avgWeek: !avgWeek || _.isNaN(avgWeek) ? 0 : avgWeek,

        rateCurrentMonth: rateCurrentMonth,
        avgMonth: !avgMonth || _.isNaN(avgMonth) ? 0 : avgMonth
    }

    return result
}

const getCompletionRateDaily = (habit, yearCalendar, iso = false) => {
    let today = moment().endOf('day')
    let minDate = getStartTime(habit.startDate)
    let maxDate = moment(today.toDate()).endOf('month')
    let diffDates = moment.duration(today.diff(minDate)).as('days')
    let currentWeek = getWeekOfYear(today, iso)

    let expectCheckinOfWeek = habitGetExpectCheckin(habit, moment())
    let tmpMonths = {}
    let labelWeek = []
    let dataWeek = []

    let labelMonth = []
    let dataMonth = []
    let tmpResult = {}
    let tmpRateResult = {}
    let currentLang = storage.get('locale', 'en')
    
    // let moduleLang = language[currentLang]

    for (let _day = 0; _day < diffDates; _day++) {
        let tmp = moment(minDate.toDate()).startOf('day').add(_day, 'days')
        let status = isHabitDone(habit, tmp)
        let _year = parseInt(tmp.format('YYYY'))
        let _month = parseInt(tmp.format('MM'))
        let _monthName = tmp.format('MMMM').toLowerCase()
        let _weekOfYear = getWeekOfYear(tmp, iso)
        let _increaseCount = status ? 1 : 0

        if (!tmpResult.hasOwnProperty(_year)) {
            tmpResult[_year] = {}
            tmpRateResult[_year] = {}
        }

        if (!tmpResult[_year].hasOwnProperty(_month)) {
            tmpResult[_year][_month] = {}
            tmpRateResult[_year][_month] = {}
        }

        if (!tmpResult[_year][_month].hasOwnProperty(_weekOfYear)) {
            tmpResult[_year][_month][_weekOfYear] = 0
            tmpRateResult[_year][_month][_weekOfYear] = 0
        }
        tmpResult[_year][_month][_weekOfYear] += _increaseCount

        _monthName = _monthName.toLowerCase() === 'january' ? _year : tmp.locale(currentLang).format('MMMM').toLowerCase()
        if (labelMonth[labelMonth.length - 1] !== _monthName) {
            labelMonth.push(_monthName)
        }
    }

    //Loop
    let yearKeys = Object.keys(tmpResult)
    yearKeys.forEach(year => {
        let monthKeys = Object.keys(tmpResult[year])
        let yearKey = parseInt(year)
        monthKeys.forEach(month => {
            let monthKey = parseInt(month)
            let monthData = tmpResult[year][month]
            let weekKeys = Object.keys(monthData)

            let rateMonth = 0
            weekKeys.forEach(week => {
                let weekRates = (monthData[week]/expectCheckinOfWeek)*100

                // add week label
                let weekLabel = week <= 1 ? year : 'W' + week
                // if (labelWeek[labelWeek.length - 1] !== weekLabel) {
                //     labelWeek.push(weekLabel)

                //     dataWeek.push(weekRates)
                //     rateMonth += weekRates
                // }
                if (labelWeek[labelWeek.length - 1] === weekLabel) {
                    rateMonth += weekRates
                    dataWeek[dataWeek.length - 1] += weekRates
                } else {
                    labelWeek.push(weekLabel)
                    dataWeek.push(weekRates)
                    rateMonth += weekRates
                }
                tmpRateResult[year][month][week] = weekRates
            })

            // dataMonth.push(rateMonth/weekKeys.length) 
            let startOfMonth = moment(`${monthKey > 10 ? monthKey : '0' + monthKey}${yearKey}`, 'MMYYYY').startOf('month').startOf(iso ? 'W' : 'w').startOf('day')
            let endOfMonth = moment(`${monthKey > 10 ? monthKey : '0' + monthKey}${yearKey}`, 'MMYYYY').endOf('month').endOf(iso ? 'W' : 'w').endOf('day')
            let durationWeeks = moment.duration(endOfMonth.diff(startOfMonth)).as('weeks')
                durationWeeks = Math.round(durationWeeks)
            dataMonth.push(rateMonth/durationWeeks)
        })
    })

    //rate current week
    let rateCurrentWeek = dataWeek[dataWeek.length - 1]
    let avgWeek = _.mean(dataWeek)

    let rateCurrentMonth = dataMonth[dataMonth.length - 1]
    let avgMonth = _.mean(dataMonth)

    //rate current month
    // let startDayOfMonth = moment(today.toDate()).startOf('month')
    // let diffToStartMonth = moment.duration(today.diff(startDayOfMonth)).as('days')
    // let checkedIn = 0
    // for (let _day = 0; _day < diffToStartMonth; _day++) {
    //     let _tmpDate = moment(startDayOfMonth.toDate()).add(_day, 'days')
    //     if (isHabitDone(habit, _tmpDate))
    //         checkedIn++
    // }

    // let rateCurrentMonth = (checkedIn/(diffToStartMonth*expectCheckinOfWeek)) * 100
    // let avgMonth = _.mean(dataMonth)
    // dataMonth[dataMonth.length - 1] = parseFloat(rateCurrentMonth).toFixed(1)

    let result = {
        dayDatas: tmpResult,
        dayRateDatas: tmpRateResult,

        dataWeek: dataWeek,
        labelWeek: labelWeek,

        labelMonth: labelMonth,
        dataMonth: dataMonth,

        rateCurrentWeek: rateCurrentWeek,
        avgWeek: !avgWeek || _.isNaN(avgWeek) ? 0 : avgWeek,

        rateCurrentMonth: rateCurrentMonth,
        avgMonth: !avgMonth || _.isNaN(avgMonth) ? 0 : avgMonth
    }

    return result
}

const getCompletionRateDailyNew = (habit, yearCalendar, iso = false) => {
    let today = moment().endOf('day')
    let minDate = getStartTime(habit.startDate)
    let maxDate = moment(today.toDate()).endOf('month')
    let diffDates = moment.duration(today.diff(minDate)).as('days')
    let currentWeek = getWeekOfYear(today, iso)

    let expectCheckinOfWeek = habitGetExpectCheckin(habit, moment())
    let tmpMonths = {}
    let labelWeek = []
    let dataWeek = []

    let labelMonth = []
    let dataMonth = []
    let tmpResult = {}

    let tmpResult1 = {}
    let labelMonth1 = []
    let labelWeek1 = []
    let dataWeek1 = []
    let dataMonth1 = []
    let dataWeekOfMonth = {}
    if (!yearCalendar || !Object.keys(yearCalendar).length) {
        return {
            dayDatas: {},

            dataWeek: [],
            labelWeek: [],

            labelMonth: [],
            dataMonth: [],

            rateCurrentWeek: 0,
            avgWeek: 0,

            rateCurrentMonth: 0,
            avgMonth: 0 
        }
    }

    let currentLang = storage.get('locale', 'en')
    let yearCalendarKeys = Object.keys(yearCalendar)
    yearCalendarKeys.forEach(_year => {
        let yearKey = parseInt(_year)
        dataWeekOfMonth[yearKey] = {}

        let monthKeys = Object.keys(yearCalendar[_year])
        monthKeys.forEach(_month => {
            let dayOfMonth = yearCalendar[_year][_month].month
            let monthKey = parseInt(_month)
            dataWeekOfMonth[yearKey][monthKey] = {}

            let days = yearCalendar[_year][_month].days
            days.forEach(day => {
                let tmp = moment(day.day.toDate()).startOf('day')
                let status = day.status
                let _year = parseInt(tmp.format('YYYY'))
                let _month = parseInt(tmp.format('MM'))
                let _weekOfYear = getWeekOfYear(tmp, iso)
                let _increaseCount = status === habitStatus.done ? 1 : 0
            
                if (
                    parseInt(tmp.format('X')) < parseInt(minDate.format('X')) 
                    || parseInt(tmp.format('X')) > parseInt(moment().startOf('day').format('X'))
                )
                    return false

                if (!tmpResult1.hasOwnProperty(_year)) {
                    tmpResult1[_year] = {}
                }
        
                if (!tmpResult1[_year].hasOwnProperty(_month)) {
                    tmpResult1[_year][_month] = {}
                }
        
                if (!tmpResult1[_year][_month].hasOwnProperty(_weekOfYear)) {
                    tmpResult1[_year][_month][_weekOfYear] = 0
                }
                tmpResult1[_year][_month][_weekOfYear] += _increaseCount
            })

            if (!tmpResult1 || !Object.keys(tmpResult1).length)
                return
            // generate month name
            let tmp = moment(yearCalendar[_year][_month].month.toDate()).locale(currentLang)
            let _monthName = moment(yearCalendar[_year][_month].month.toDate()).locale('en').format('MMMM').toLowerCase()
            _monthName = _monthName === 'january' ? _year : tmp.format('MMMM').toLowerCase()
            if (labelMonth1[labelMonth1.length - 1] !== _monthName) {
                labelMonth1.push(_monthName)
            }

            let monthData = tmpResult1[yearKey][monthKey]
            if (!monthData)
                return
            let weekKeys = Object.keys(monthData)
            let rateMonth = 0
            weekKeys.forEach(week => {
                let weekRates = (monthData[week]/expectCheckinOfWeek)*100
                
                // add week label
                let weekLabel = week <= 1 ? _year : 'W' + week
                dataWeekOfMonth[yearKey][monthKey][week] = {
                    label: weekLabel,
                    rate: weekRates,
                    month: _month,
                    year: _year,
                }

                if (labelWeek1[labelWeek1.length - 1] === weekLabel) {
                    rateMonth += weekRates
                    dataWeek1[dataWeek1.length - 1] += weekRates
                } else {
                    labelWeek1.push(weekLabel)
                    dataWeek1.push(weekRates)
                    rateMonth += weekRates
                }
            })

            let startOfMonth = moment(dayOfMonth.toDate()).startOf('month').startOf(iso ? 'W' : 'w').startOf('day')
            let endOfMonth = moment(dayOfMonth.toDate()).endOf('month').endOf(iso ? 'W' : 'w').endOf('day')
            let durationWeeks = moment.duration(endOfMonth.diff(startOfMonth)).as('weeks')
                durationWeeks = Math.round(durationWeeks)

            dataMonth1.push(rateMonth/durationWeeks)
        })
    })

    let rateCurrentWeek = dataWeek1[dataWeek1.length - 1]
    let avgWeek = _.mean(dataWeek1)

    let rateCurrentMonth = dataMonth1[dataMonth1.length - 1]
    let avgMonth = _.mean(dataMonth1)

    let result = {
        dayDatas: tmpResult1,

        dataWeek: dataWeek1,
        labelWeek: labelWeek1,

        labelMonth: labelMonth1,
        dataMonth: dataMonth1,

        rateCurrentWeek: rateCurrentWeek,
        avgWeek: !avgWeek || _.isNaN(avgWeek) ? 0 : avgWeek,

        rateCurrentMonth: rateCurrentMonth,
        avgMonth: !avgMonth || _.isNaN(avgMonth) ? 0 : avgMonth,

        dataWeekOfMonth: dataWeekOfMonth
    }

    return result
}

const sortNoteList = (notes) => {
    let noteKeys = Object.keys(notes)
    let noteArray = []
    noteKeys.forEach(noteId => {
        let tmpObj = notes[noteId]
        tmpObj['miliseconds'] = parseInt(moment(tmpObj.created, dateConfigFormat.note).format('x'))
        tmpObj['key'] = noteId
        noteArray.push(tmpObj)
    })
    if (noteArray.length <= 0)
        return []
    return _.orderBy(noteArray, ['miliseconds'], ['asc'])
}

const getSystemCurrency = () => {
    let langs = (navigator.language).split('-')
    let lang = langs[0]
    let currency = langs.length > 1 ? langs[1] : 'US'
    let listLang = Object.keys(customCountry)
    let listCurrenry = Object.values(customCountry)
    let index = listCurrenry.indexOf(currency)

    let resultCurrency = index >= 0 ? currency : 'US'
    return resultCurrency
}

const getCurrencrPrefix = (_currency) => {
    let listCurrenry = Object.keys(currencyPrefix)
    let index = listCurrenry.indexOf(_currency)

    let resultCurrency = index >= 0 ? currencyPrefix[_currency] : currencyPrefix.US
    return resultCurrency
}

const isPremium = (userData) => {
    if (!userData || !Object.keys(userData).length) {
        return false
    }
    
    let today = moment()
    let expiredDate = userData.hasOwnProperty('premiumExpireDate') ? userData.premiumExpireDate : null
    let premiumAndroid = userData.hasOwnProperty('premiumStatusAndroid') ? parseInt(userData.premiumStatusAndroid) : premiumStatus.androidFree
    let premiumIos = userData.hasOwnProperty('premiumStatus') ? parseInt(userData.premiumStatus) : premiumStatus.iosFree

    let flag = false
    if (premiumAndroid === premiumStatus.androidPremium || premiumIos === premiumStatus.iosPremium) {
        flag = true
        console.log('lifetime')
    } else if (expiredDate) {
        let tmpExpired = parseInt(moment(expiredDate, dateConfigFormat.FORMAT_EXPIRED_DATE).format('X'))
        let tmpToday = parseInt(today.format('X'))

        console.log(tmpToday, tmpExpired, tmpToday < tmpExpired)
        flag = tmpToday < tmpExpired
    }

    return flag
}

const getGreetingTime =  () => {
	var g = null; //return g
	
	var split_afternoon = 12 //24hr time to split the afternoon
	var split_evening = 17 //24hr time to split the evening
	var currentHour = parseInt(moment().format("HH"));
    
	if(currentHour >= split_afternoon && currentHour <= split_evening) {
		g = timeOfDay.afternoon
	} else if(currentHour >= split_evening) {
		g = timeOfDay.night
	} else {
		g = timeOfDay.morning
    }
    
	return g;
}

// Premium feature
const userCanSkip = (userPremiumData) => {
    let premiumStatus = isPremium(userPremiumData)

    return premiumStatus
}

const userCanArchive = (userPremiumData) => {
    let premiumStatus = isPremium(userPremiumData)

    return premiumStatus
}

const userCanAddReminder = (userPremiumData) => {
    let premiumStatus = isPremium(userPremiumData)
    return premiumStatus
}

const compareSameObjectData = (first, last) => {
    return JSON.stringify(first) === JSON.stringify(last)
}

export default {
    // date time
    isInFeature: isInFeature,
    isOutOfRangeDate: isOutOfRangeDate,
    getStartTime: getStartTime,
    getFirstDayOfWeek: getFirstDayOfWeek,
    getEndDayOfWeek: getEndDayOfWeek,
    getWeekOfYear: getWeekOfYear,
    getCurrentTime: getCurrentTime,
    getCalendarRange: getCalendarRange,
    isEqualDateWithFormat: isEqualDateWithFormat,
    getGreetingTime: getGreetingTime,
    getListDropdown: getListDropdown,

    getWeeklyCount: getWeeklyCount,
    getWeekDaysCount: getWeekDaysCount,
    getRegularlyCountFromString: getRegularlyCountFromString,

    getTotalActualCheckInThisWeek: getTotalActualCheckInThisWeek,
    isHabitDone: isHabitDone,
    
    
    getIsArchived: getIsArchived,
    regularlyIsValid: regularlyIsValid,
    getHabitStatus: getHabitStatus,

    getTriggerTimes: getTriggerTimes,
    getTriggerLeft: getTriggerLeft,
    dateIsFeatureOrPast: dateIsFeatureOrPast,

    // habit
    habitCheckedInDays: habitCheckedInDays,
    habitConsecutiveDailyCheckin: habitConsecutiveDailyCheckin,
    habitConsecutiveWeekDaysCheckin: habitConsecutiveWeekDaysCheckin,
    habitConsecutiveIntervalCheckin: habitConsecutiveIntervalCheckin,
    habitConsecutiveWeeklyCheckin: habitConsecutiveWeeklyCheckin,
    habitGetExpectCheckin: habitGetExpectCheckin,

    //user prefercenses
    pIsStartWithMonday: pIsStartWithMonday,
    getWeekDays: getWeekDays,
    getMomentWeekDays: getMomentWeekDays,
    getMomentWeekDay: getMomentWeekDay,

    streaksHabitDaily: streaksHabitDaily,
    streaksHabitWeekdays: streaksHabitWeekdays,
    streaksHabitDayInterval: streaksHabitDayInterval,
    streaksHabitDayWeekly: streaksHabitDayWeekly,

    isPremium: isPremium,

    //generate
    generateRequiresInterval: generateRequiresInterval,

    //completion rate
    getCompletionRateDaily: getCompletionRateDaily,
    getCompletionRateDailyNew: getCompletionRateDailyNew,

    // sort
    sortNoteList: sortNoteList,

    // system lang
    getSystemCurrency: getSystemCurrency,
    getCurrencrPrefix: getCurrencrPrefix,

    // premium control
    userCanSkip: userCanSkip,
    userCanArchive: userCanArchive,
    userCanAddReminder: userCanAddReminder,

    //utitils
    compareSameObjectData: compareSameObjectData,
}