import { mapGetters } from 'vuex'
import * as configs from'@/configs'
import appUtils from '@/utils/appUtils'

export const slugString = {

}

export const seoMeta = {
    methods: {
        generateSeo (title, description, keyword = '') {
            return [
                {property: 'og:title', content: title},
                {property: 'og:site_name', content: title},
                // The list of types is available here: http://ogp.me/#types
                {property: 'og:type', content: 'website'},
                // Should the the same as your canonical link, see below.
                {property: 'og:url', content: window.location.href},
                {property: 'og:image', content: 'https://www.my-site.com/my-special-image.jpg'},
                // Often the same as your meta description, but not always.
                {property: 'og:description', content: description},

                // Twitter card
                {name: 'twitter:card', content: 'summary'},
                {name: 'twitter:site', content: window.location.href},
                {name: 'twitter:title', content: title},
                {name: 'twitter:description', content: description},
                // Your twitter handle, if you have one.
                {name: 'twitter:creator', content: '@habitify'},
                {name: 'twitter:image:src', content: 'https://www.my-site.com/my-special-image.jpg'},

                // Google / Schema.org markup:
                {itemprop: 'name', content: title},
                {itemprop: 'keyword', content: keyword},
                {itemprop: 'description', content: description},
                {itemprop: 'image', content: 'https://www.my-site.com/my-special-image.jpg'}
            ]
        }
    }
}

export const checkHabit = {
    methods: {
        /**
         * @habit: Object - Habit object
         * @date: Date - with format: dd/mm/yyyy
         * */
        isHabitDoneToday (habit, date) {
            if (!habit || (habit && !habit.hasOwnProperty('checkins'))) {
                return false
            }

            if (!Object.keys(habit.checkins).length) {
                return false
            }

            let dateCheckins = Object.keys(habit.checkins)
            return dateCheckins.indexOf(date) >= 0 && habit.checkins[date] === configs.habitStatus.done
        }
    }
}

export const dateMixins = {
    methods: {
        getNameOfCurrentDate (currentDate, lang) {
            let currentDateFMT = currentDate.format(configs.momentDD_MM_YYYY)
            let todayFMT = moment().format(configs.momentDD_MM_YYYY)
            let yesterdayFMT = moment().subtract(1, 'day').format(configs.momentDD_MM_YYYY)
            let tomorrowFMT = moment().add(1, 'day').format(configs.momentDD_MM_YYYY)
            let nameCr = ''
            let currentDateYear = currentDate.format('YYYY')
            let todayYear = moment().format('YYYY')

            if (currentDateFMT === todayFMT) {
                nameCr = this.$t('common.today')
            } else if (currentDateFMT === yesterdayFMT) {
                nameCr = this.$t('common.yesterday')
            } else if (currentDateFMT === tomorrowFMT) {
                nameCr = this.$t('common.tomorrow')
            } else {
                nameCr = currentDate.locale(lang).format(currentDateYear === todayYear ? configs.dateConfigFormat.SAME_YEAR : configs.dateConfigFormat.DIFF_YEAR)
            }

            return nameCr
        },
        momentFormat (date, format) {
            return date.format(format)
        },
        momentToDate (date) {
            return date.toDate()
        },
        momentParseFromDate (date) {
            return moment(date)
        }
    }
}

export const habitMixins = {
    computed: {
        ...mapGetters(['iso'])
    },
    methods: {
        getHabitByDay (currentDate, habits, timeOfDay, includeTomorow = false, includeDone = true) {
            if (!habits || !Object.keys(habits).length) {
                return {};
            }
            let keys = Object.keys(habits)
            let result = {}

            let checkined = []
            let notChecked = []

            keys.forEach(key => {
                let _habit = habits[key]
                let regularly = _habit.regularly

                let dayOfWeek = moment(currentDate.toDate()).locale('en').format('ddd').toLowerCase()

                // ignore if habit in feature or is archive
                if (
                    appUtils.isOutOfRangeDate(_habit, currentDate, includeTomorow) ||
                    appUtils.getIsArchived(_habit)
                ) {
                    return;
                }

                //ignore if diff time of day
                if (timeOfDay !== configs.timeOfDay.anyTime && _habit.timeOfDay !== configs.timeOfDay.anyTime && timeOfDay !== _habit.timeOfDay) {
                    return;
                }

                let isDoneOnInputDate = appUtils.getHabitStatus(_habit, currentDate) !== configs.habitStatus.none
                
                // check if Not include done, and status of input date is done, return
                if (!includeDone && isDoneOnInputDate)
                    return false

                if (isDoneOnInputDate) {
                    result[key] = _habit
                } else if (regularly === configs.regularly.daily) { // filter for daily
                    result[key] = _habit
                } else if (regularly.indexOf(dayOfWeek) >= 0) { // filter for weekDays
                    result[key] = _habit
                } else if (regularly.indexOf(configs.regularly.weekly) >= 0) { // filter for weekly
                    let weeklyCount = appUtils.getWeeklyCount(regularly)
                    let startDateOfWeek = appUtils.getFirstDayOfWeek(currentDate, this.iso)
                    let totalCheckInThisWeek = appUtils.getTotalActualCheckInThisWeek(_habit, startDateOfWeek)
                    if (weeklyCount > totalCheckInThisWeek) {
                        result[key] = _habit
                    }
                } else if (regularly.indexOf(configs.regularly.interval) >= 0) {
                    if (appUtils.regularlyIsValid(_habit, currentDate)) {
                        result[key] = _habit
                    }
                }

                checkined.push({key: key, priority: _habit.priority, sort: appUtils.getHabitStatus(_habit, currentDate)})
            })

            checkined = _.sortBy(checkined, ['sort', 'priority'])
            let resultList = []
            checkined.forEach((index) => {
                if (!result.hasOwnProperty(index.key)) {
                    return
                }
                let _tmp = {
                    key: index.key,
                    content: result[index.key]
                }
                resultList.push(_tmp)
            })

            return resultList
        },
        getHabitStatusByDay (habit, currentDate) {
            return appUtils.getHabitStatus(habit, currentDate)
        },
        getExpectCheckinOfWeek (habit, currentDate) {
            return appUtils.habitGetExpectCheckin(habit, currentDate)
        },
        getActualCheckinOfWeek (habit, currentDate) {
            if (!habit || !Object.keys(habit).length) {
                return 0
            }

            let _actualDays = 0
            let _regularly = habit.regularly
            let _currentDate = moment(currentDate.toDate()).startOf('day')
            let _startDateOfWeek = moment(currentDate.toDate()).startOf('week')
            let diffFromNow = moment.duration(_currentDate.diff(_startDateOfWeek)).as('days') + 1
            let _startDate = appUtils.getStartTime(habit.startDate).startOf('day')

            if (_regularly === configs.regularly.daily) {
                for (let i = 0; i < diffFromNow; i++) {
                    let tmp = moment(currentDate.toDate()).subtract(i, 'days')
                    if (appUtils.dateIsFeatureOrPast(habit.startDate, tmp))
                        continue

                    if (appUtils.getHabitStatus(habit, tmp) === configs.habitStatus.done)
                        _actualDays++
                }
            } else if (_regularly.indexOf(configs.regularly.weekdays) >= 0) { // if weekDays
                for (let i = 0; i < diffFromNow; i++) {
                    let tmp = moment(currentDate.toDate()).subtract(i, 'days')
                    let statusOfDate = appUtils.getHabitStatus(habit, tmp)
                    if (appUtils.dateIsFeatureOrPast(habit.startDate, tmp))
                        continue

                    if (_regularly.indexOf(tmp.format('ddd').toLowerCase()) >= 0 && statusOfDate === configs.habitStatus.done)
                        _actualDays++
                    else if (statusOfDate === configs.habitStatus.done)
                        _actualDays++
                }
            } else if (_regularly.indexOf(configs.regularly.weekly) >= 0) { // if weekly
                for (let i = 0; i < diffFromNow; i++) {
                    let tmp = moment(currentDate.toDate()).subtract(i, 'days')
                    if (appUtils.dateIsFeatureOrPast(habit.startDate, tmp))
                        continue

                    if (appUtils.getHabitStatus(habit, tmp) === configs.habitStatus.done)
                        _actualDays++
                }
            } else if (_regularly.indexOf(configs.regularly.interval) >= 0) { // if interval
                for (let i = 0; i < diffFromNow; i++) {
                    let tmp = moment(currentDate.toDate()).subtract(i, 'days')
                    
                    if (appUtils.dateIsFeatureOrPast(habit.startDate, tmp))
                        continue
                        
                    if (appUtils.getHabitStatus(habit, tmp) === configs.habitStatus.done)
                        _actualDays++
                }
            }

            return _actualDays
        },
        getConsecutiveDays (habit, currentDate) {
            if (!habit || !Object.keys(habit).length) {
                return 0
            }

            let _consecutiveDays = 0
            let _regularly = habit.regularly
            let _currentDate = moment(currentDate.toDate()).startOf('day')
            let _minDate = appUtils.getStartTime(habit.startDate)

            if (_regularly === configs.regularly.daily) {
                _consecutiveDays = appUtils.habitConsecutiveDailyCheckin(habit, _currentDate)
            } else if (_regularly.indexOf(configs.regularly.weekdays) >= 0) { // if weekDays
                _consecutiveDays = appUtils.habitConsecutiveWeekDaysCheckin(habit, _currentDate, this.iso)
            } else if (_regularly.indexOf(configs.regularly.weekly) >= 0) { // if weekly
                _consecutiveDays = appUtils.habitConsecutiveWeeklyCheckin(habit, _currentDate, this.iso)
            } else if (_regularly.indexOf(configs.regularly.interval) >= 0) { // if interval
                _consecutiveDays = appUtils.habitConsecutiveIntervalCheckin(habit, _currentDate, this.iso)
            }

            return _consecutiveDays
        },
        getStreaksFromStart (habit, currentDate) {
            if (!habit || !Object.keys(habit).length) {
                return []
            }

            let _streakList = []
            let _regularly = habit.regularly
            let _currentDate = moment(currentDate.toDate()).startOf('day')
            let _minDate = appUtils.getStartTime(habit.startDate)
            let diffDays = moment.duration(_currentDate.diff(_minDate)).as('days')

            //.daily, .dayInterval(_), .everyWeekDay(_):
            let result = {}
            if (_regularly === configs.regularly.daily) {
                result = appUtils.streaksHabitDaily(habit, _currentDate)
            } else if (_regularly.indexOf(configs.regularly.weekdays) >= 0) { // if weekDays
                result = appUtils.streaksHabitWeekdays(habit, _currentDate)
            } else if (_regularly.indexOf(configs.regularly.interval) >= 0) { // if interval
                result = appUtils.streaksHabitDayInterval(habit, _currentDate)
            } else if (_regularly.indexOf(configs.regularly.weekly) >= 0) { // if weekly
                result = appUtils.streaksHabitDayWeekly(habit, _currentDate, this.iso)
            }

            return result
        },
        getCompletionRate (habit, yearCalendar) {
            // generate week and month from start
            return appUtils.getCompletionRateDaily(habit, yearCalendar, this.iso)
            
        },
        getCompletionRateNew (habit, yearCalendar) {
            // generate week and month from start
            return appUtils.getCompletionRateDailyNew(habit, yearCalendar, this.iso)
            
        },
        flatRegularly (lang, format) {
            return {
                daily: {label: this.$i18n.t('common.everyday'), value: 'daily'},
                'weekdays': {label: this.$i18n.t('common.weekdays'), value: 'weekDays-mon,tue,wed,thu,fri'},
                'weekend': {label: this.$i18n.t('common.weekends'), value: 'weekDays-sat,sun'},
                'weekDays-mon': {label: this.getWeekDays(0, lang, format), value: 'weekDays-mon'},
                'weekDays-tue': {label: this.getWeekDays(1, lang, format), value: 'weekDays-tue'},
                'weekDays-wed': {label: this.getWeekDays(2, lang, format), value: 'weekDays-wed'},
                'weekDays-thu': {label: this.getWeekDays(3, lang, format), value: 'weekDays-thu'},
                'weekDays-fri': {label: this.getWeekDays(4, lang, format), value: 'weekDays-fri'},
                'weekDays-sat': {label: this.getWeekDays(5, lang, format), value: 'weekDays-sat'},
                'weekDays-sun': {label: this.getWeekDays(6, lang, format), value: 'weekDays-sun'},
                'weekly-1': {label: this.$i18n.tc('plurals.day.per.week', 1, {num: 1}), value: 'weekly-1'},
                'weekly-2': {label: this.$i18n.tc('plurals.day.per.week', 2, {num: 2}), value: 'weekly-2'},
                'weekly-3': {label: this.$i18n.tc('plurals.day.per.week', 3, {num: 3}), value: 'weekly-3'},
                'weekly-4': {label: this.$i18n.tc('plurals.day.per.week', 4, {num: 4}), value: 'weekly-4'},
                'weekly-5': {label: this.$i18n.tc('plurals.day.per.week', 5, {num: 5}), value: 'weekly-5'},
                'weekly-6': {label: this.$i18n.tc('plurals.day.per.week', 6, {num: 6}), value: 'weekly-6'},
                'dayInterval-2': {label: this.$i18n.tc('plurals.interval.day', 2, {num: 2}), value: 'dayInterval-2'},
                'dayInterval-3': {label: this.$i18n.tc('plurals.interval.day', 3, {num: 3}), value: 'dayInterval-3'},
                'dayInterval-4': {label: this.$i18n.tc('plurals.interval.day', 4, {num: 4}), value: 'dayInterval-4'},
                'dayInterval-5': {label: this.$i18n.tc('plurals.interval.day', 5, {num: 5}), value: 'dayInterval-5'},
                'dayInterval-6': {label: this.$i18n.tc('plurals.interval.day', 6, {num: 6}), value: 'dayInterval-6'},
                'dayInterval-7': {label: this.$i18n.tc('plurals.interval.day', 7, {num: 7}), value: 'dayInterval-7'},
            }
        },
        getWeekDays (dow, lang, format) {
            moment.locale(lang)
            // let dayList = moment.weekdays(this.iso)
            let dayList = appUtils.getMomentWeekDays(lang, this.iso, format)
            return _.capitalize(dayList[dow])
        },
        getCurrentRegularlyText (_regularly, _lang) {
            let displayName = null
            if (_regularly.indexOf('weekDays-') >= 0) {
                let listWeekday = _regularly.replace(/weekDays-/g, '')
                listWeekday = listWeekday.split(',').sort()

                // if exists define
                let existsDefine = null
                if (listWeekday.length === 2 && listWeekday.indexOf('sun') >= 0 && listWeekday.indexOf('sat') >= 0) // weekend
                    existsDefine = this.flatRegularlyList.weekend
                else if (listWeekday.length === 5 && listWeekday.indexOf('sun') < 0 && listWeekday.indexOf('sat') < 0) // weekdays
                    existsDefine = this.flatRegularlyList.weekdays
                
                if (existsDefine) {
                    displayName = existsDefine.label
                    return displayName
                }
                
                let dayValid = []
                let weekdays = appUtils.getMomentWeekDays(_lang, this.iso, 'full')
                let weekdayShorts = appUtils.getMomentWeekDays(_lang, this.iso, 'short')
                listWeekday.forEach(day => {
                    let objDay = this.flatRegularlyList['weekDays-' + day]
                    if (objDay) {
                        dayValid.push(objDay.label)
                    }
                })

                let labelSort = []
                weekdays.forEach((day, key) => {
                    if (dayValid.indexOf(_.capitalize(day)) >= 0)
                        labelSort.push(weekdayShorts[key])
                })

                displayName = labelSort.join(', ')
            } else {
                let obj = this.flatRegularlyList[_regularly]
                if (obj)
                    displayName = obj.label
            }

            return displayName
        }
    }
}
