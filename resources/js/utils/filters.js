import { default as numeral } from 'numeral'
import { habitStatus, regularly, dateConfigFormat } from '@/configs'
import appUtils from '@/utils/appUtils'
import app from '@/app'

export function numberFormat (value, format) {
    return numeral(value).format(format)
}

export function dateFormat (value, format) {
    return moment(value).format(format)
}

export function habitFormatTimeTriggers (habit, currentDate, iso = false, lang = 'en') {
    let _currentDate = moment(currentDate.toDate())
    if (!habit || !habit.remind || !habit.remind.hasOwnProperty('timeTriggers')) {
        return null
    }

    let times = appUtils.getTriggerTimes(habit.remind.timeTriggers)
    let last = times[times.length - 1];

    let convert = moment(last, 'HH:mm').locale('en').format('HH:mm')
    let left = times.length - 1
    let textLeft = left > 0 ? app.$i18n.tc('plurals.more', left, {num: left}) : null
    return left > 0 ? [' - ', convert,' + ', textLeft].join('') : [' - ', convert].join('')


    let _regularly = habit.regularly
    // daily or weekdays
    let results = []
    let display = ""
    if (
        _regularly === regularly.daily ||
        _regularly.indexOf(regularly.weekdays) >= 0 ||
        _regularly.indexOf(regularly.interval) >= 0
    ) {
        results = appUtils.getTriggerTimes(habit.remind.timeTriggers)
        
        display = results.join(', ')
    } else if (_regularly.indexOf(regularly.weekly) >= 0) {
        let _triggers = appUtils.getTriggerTimes(habit.remind.timeTriggers)
        let firstDayOfWeek = appUtils.getFirstDayOfWeek(_currentDate, iso)
        let _count = appUtils.getTriggerLeft(_regularly, habit, firstDayOfWeek)
        let _suffix = _count > 1 ? 's' : ''
        display = _triggers.join(', ')
        display = _count <= 0 ? display : display + ' - ' + app.$i18n.t('plurals.more', count, {num: _count})
    }

    return display
}

export function displayNoteTime(value, lang) {
    let date = moment(value, dateConfigFormat.note)
    return date.locale(lang).format('LLLL')//`${date.format('HH:mm')} ngày ${date.format('DD')} tháng ${date.format('MM')}, ${date.format('YYYY')}`
}

export function displayFloatToFixed(value, fixed = 1) {
    let _value = !value || _.isNaN(value) || _.isNull(value) ? 0 : value
    return parseFloat(_value).toFixed(fixed)
}

export function displayManagerHabitHour(value) {
    let splits = value.split(':')
    let hour = parseInt(splits[0])
    let minute = parseInt(splits[1])
    // let suffix = 'AM'
    // suffix = hour > 12 ? 'PM' : 'AM'
    // hour = hour > 12 ? hour - 12 : hour
    hour = hour < 10 ? '0' + hour : hour
    minute = minute < 10 ? '0' + minute : minute

    return `${hour}:${minute}`
}