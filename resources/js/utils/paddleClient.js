import { paddleConfigs } from '@/configs'
import PaddleSDK from '@/plugins/paddle-sdk/sdk'

const paddleClient = new PaddleSDK()

export default function install (Vue) {
    Object.defineProperties(Vue.prototype, {
        $paddle: {
            get () {
                return paddleClient
            }
        }
    })
}