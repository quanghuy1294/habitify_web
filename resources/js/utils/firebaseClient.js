import { firebaseConfigs, firebaseConfigsProd } from '@/configs'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/messaging'
import 'firebase/storage'
const listConfigs = {
    firebaseConfigs: firebaseConfigs,
    firebaseConfigsProd: firebaseConfigsProd
}
const firebaseClient = firebase.initializeApp(listConfigs[process.env.MIX_FIREBASE_CLIENT_TYPE])

export default function install (Vue) {
    Object.defineProperties(Vue.prototype, {
        $firebase: {
            get () {
                return firebaseClient
            }
        }
    })
}