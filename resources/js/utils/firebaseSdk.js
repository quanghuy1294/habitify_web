import { firebaseConfigs, firebaseConfigsProd } from '@/configs'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/messaging'
import 'firebase/storage'
const listConfigs = {
    firebaseConfigs: firebaseConfigs,
    firebaseConfigsProd: firebaseConfigsProd
}
firebase.initializeApp(listConfigs[process.env.MIX_FIREBASE_CLIENT_TYPE])

const firebaseClient = firebase

export default function install (Vue) {
    Object.defineProperties(Vue.prototype, {
        $firebase: {
            get () {
                return firebaseClient
            }
        }
    })
}