import * as types from '../mutation-types'
import * as configs from '@/configs'
import moment from 'moment'
import Vue from 'vue'

const state = {
    selectedHabit: {},
    habitEditManager: {},
}

const getters = {
    selectedHabit: state => state.selectedHabit,
    habitEditManager: state => state.habitEditManager,
}

const actions = {
    setSelectedHabit ({ commit, state }, habit) {
        commit(types.SET_SELECTED_MANAGER_HABIT, habit)
    },
    setHabitEditManager ({ commit, state }, habit) {
        commit(types.SET_HABIT_EDIT_MANAGER, habit)
    },

    deleteHabitEditManagerReminder ({ commit, state }, hour) {
        commit(types.DELETE_HABIT_EDIT_MANAGER_REMINDER, hour)
    },
}

const mutations = {
    [types.SET_SELECTED_MANAGER_HABIT] (state, habit) {
        Vue.set(state, 'selectedHabit', habit ? habit : {})
    },
    [types.SET_HABIT_EDIT_MANAGER] (state, habit) {
        Vue.set(state, 'habitEditManager', habit ? habit : {})
    },
    [types.DELETE_HABIT_EDIT_MANAGER_REMINDER] (state, hour) {
        let newObj = {}
        let keys = Object.keys(state.habitEditManager.content.remind.timeTriggers)
        keys.forEach(keyHour => {
            if (hour === keyHour)
                return false
            newObj[keyHour] = state.habitEditManager.content.remind.timeTriggers[keyHour]
        })
        
        state.habitEditManager.content.remind.timeTriggers = newObj
        Vue.set(state.habitEditManager.content.remind, 'timeTriggers', newObj)
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}