import * as types from '../mutation-types'
import app from '@/app'
import storage from 'store2'
import { firstWeekDay } from '@/configs'
import moment from 'moment-timezone'
import Vue from 'vue'

const state = {
    user: {},
    preferences: {
        appBadgeEnabled: true,
        dailyNotificationEnabled: false,
        firstWeekDay: firstWeekDay.startMonday,
        privacyLockEnabled: false,
        repeatingNotificationEnabled: false,
        soundEnabled: true,
    },
    lang: storage.get('locale', 'en'),
    userPremiumData: {},
    notifyToken: null,
}

const getters = {
    user: state => state.user,
    preferences: state => state.preferences,
    lang: state => state.lang,
    iso: state => state.preferences && state.preferences.firstWeekDay === firstWeekDay.startMonday,
    userPremiumData: state => state.userPremiumData,
    notifyToken: state => state.notifyToken,
}

const actions = {
    setUser ({ commit, state }, user) {
        commit(types.SET_USER, user)
    },
    setPreferences ({ commit, state }, preferences) {
        commit(types.SET_PREFERENCES, preferences)
    },
    setLang ({ commit, state }, lang) {
        commit(types.SET_LANG, lang)
    },
    setUserPremiumData ({ commit, state }, data) {
        commit(types.SET_USER_PREMIUM_DATA, data)
    },
    setNotifyToken ({ commit, state }, token) {
        commit(types.SET_AUTH_NOTIFY_TOKEN, token)
    },
}

const mutations = {
    [types.SET_USER] (state, user) {
        state.user = user ? user : {}
    },
    [types.SET_PREFERENCES] (state, preferences) {
        if (preferences)
            state.preferences = preferences
    },
    [types.SET_LANG] (state, lang) {
        state.lang = lang
        app.$i18n.locale = lang
        storage.set('locale', lang)
        moment.locale(lang)

        Vue.prototype.moment = moment
        window.moment = moment
    },
    [types.SET_USER_PREMIUM_DATA] (state, data) {
        state.userPremiumData = data ? data : {}
    },

    [types.SET_AUTH_NOTIFY_TOKEN] (state, token) {
        state.notifyToken = token
        Vue.set(state, 'notifyToken', token)
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}