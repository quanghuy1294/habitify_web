import * as types from '../mutation-types'
import * as configs from '@/configs'
import moment from 'moment'
import Vue from 'vue'
import storage from 'store2'
import app from '@/app'
import appUtils from'@/utils/appUtils'

const state = {
    habits: {},
    currentDate: moment(),
    today: moment(),

    currentTimeOfDay: configs.timeOfDay.anyTime,
    currentOtherTOD: appUtils.getGreetingTime(),

    currentHabit: {},
    currentHabitId: null,

    notes: {},
    originalBlankHabit: {
        name: null,
        regularly: 'daily',
        startDate: parseInt(moment().format('X')),
        timeOfDay: configs.timeOfDay.anyTime,
        isArchived: false,
        remind: {
            soundName: "Default",
            timeTriggers: {
                '9:0': true
            }
        }
    },
    blankHabit: {
        name: null,
        regularly: 'daily',
        startDate: parseInt(moment().format('X')),
        timeOfDay: configs.timeOfDay.anyTime,
        isArchived: false,
        remind: {
            soundName: "Default",
            timeTriggers: {
                '9:0': true
            }
        }
    },
    habitDirect: null,
    habitEdit: {},
    habitEditClone: {},
    currentHabitIdEdit: null
}

const getters = {
    habits: state => state.habits,
    currentDate: state => state.currentDate,
    currentDateDDMMYYYY: state => state.currentDate.format('DDMMYYYY'),
    currentTimeOfDay: state => state.currentTimeOfDay,
    currentOtherTOD: state => state.currentOtherTOD,
    currentHabit: state => state.currentHabit,
    currentHabitId: state => state.currentHabitId,
    notes: state => state.notes,

    blankHabit: state => state.blankHabit,
    originalBlankHabit: state => state.originalBlankHabit,

    habitDirect: state => state.habitDirect,

    habitEdit: state => state.habitEdit,
    habitEditClone: state => state.habitEditClone,
    currentHabitIdEdit: state => state.currentHabitIdEdit
}

const actions = {
    setHabits ({ commit, state }, habits) {
        commit(types.SET_HABITS, habits)
    },
    setCurrentDate ({ commit, state }, date) {
        commit(types.SET_CURRENT_DATE, date)
    },
    setCurrentTimeOfDay ({ commit, state }, tod) {
        commit(types.SET_CURRENT_TIME_OF_DAY, tod)
    },
    setCurrentHabit ({ commit, state }, habit) {
        commit(types.SET_CURRENT_HABIT, habit)
    },
    setNotes ({ commit, state }, notes) {
        commit(types.SET_NOTE_LIST, notes)
    },
    deleteHabitById ({ commit, state }, habitId) {
        commit(types.DELETE_HABIT_BY_ID, habitId)
    },
    updateHabitById ({ commit, state }, payload) {
        commit(types.UPDATE_HABIT_BY_ID, payload)
    },
    addHabitById ({ commit, state }, payload) {
        commit(types.ADD_HABIT_BY_ID, payload)
    },
    setDataForBlank ({ commit, state }, payload) {
        commit(types.SET_DATA_BLANK_HAIBT, payload)
    },
    addTimeTriggerForNewHabit ({ commit, state }, payload) {
        commit(types.ADD_NEW_TIME_TRIGGER, payload)
    },

    deleteNewHabitReminder ({ commit, state }, hour) {
        commit(types.DELETE_NEW_HABIT_REMINDER, hour)
    },

    setHabitDirect ({ commit, state }, habitId) {
        commit(types.SET_HABIT_DIRECT, habitId)
    },

    setCurrentHabitEdit ({ commit, state }, habit) {
        commit(types.SET_CURRENT_HABIT_EDIT, habit)
    },
    setCurrentHabitEditClone ({ commit, state }, habit) {
        commit(types.SET_CURRENT_HABIT_EDIT_CLONE, habit)
    },

    deleteHabitEditCloneReminder ({ commit, state }, hour) {
        commit(types.DELETE_HABIT_EDIT_CLONE_REMINDER, hour)
    },
}

const mutations = {
    [types.SET_HABITS] (state, habits) {
        let tmpHabits = habits === null || habits === null || !habits ? {} : habits
        state.habits = tmpHabits
    },
    [types.SET_HABIT_DIRECT] (state, habitId) {
        state.habitDirect = habitId
    },
    [types.SET_CURRENT_DATE] (state, date) {
        state.currentDate = date
        Vue.set(state, 'currentDate', date)
    },
    [types.SET_CURRENT_TIME_OF_DAY] (state, tod) {
        storage.set('timeofday', tod)
        state.currentTimeOfDay = tod
        if (tod !== configs.timeOfDay.anyTime) {
            state.currentOtherTOD = tod
        }
    },
    [types.SET_CURRENT_HABIT] (state, habit) {
        console.log('SET_CURRENT_HABIT: ', habit, habit ? 'demo' : 'abc')
        state.currentHabit = habit ? habit.content : {}
        state.currentHabitId = habit ? habit.key : null
    },
    [types.SET_NOTE_LIST] (state, notes) {
        state.notes = notes
    },
    [types.DELETE_HABIT_BY_ID] (state, habitId) {
        Vue.delete(state.habits, habitId)
    },
    [types.UPDATE_HABIT_BY_ID] (state, payload) {
        Vue.set(state.habits, payload.key, payload.value)
    },
    [types.ADD_HABIT_BY_ID] (state, payload) {
        if (!state.habits || !Object.keys(state.habits).length) {
            state.habits = payload
        }
        
        Vue.set(state.habits, payload.key, payload.value)
    },

    [types.SET_DATA_BLANK_HAIBT] (state, habit) {
        state.blankHabit = habit
    },
    [types.ADD_NEW_TIME_TRIGGER] (state, timeTrigger) {
        let newObj = {}
        newObj[timeTrigger.key] = timeTrigger.value
        if (Object.keys(state.blankHabit.remind.timeTriggers).length <= 0) {
            state.blankHabit.remind.timeTriggers = newObj
        }
        
        Vue.set(state.blankHabit.remind.timeTriggers, timeTrigger.key, timeTrigger.value)
    },
    [types.DELETE_NEW_HABIT_REMINDER] (state, hour) {
        let newObj = {}
        let keys = Object.keys(state.blankHabit.remind.timeTriggers)
        keys.forEach(keyHour => {
            if (hour === keyHour)
                return false
            newObj[keyHour] = state.blankHabit.remind.timeTriggers[keyHour]
        })
        
        state.blankHabit.remind.timeTriggers = newObj
        Vue.set(state.blankHabit.remind, 'timeTriggers', newObj)
    },

    [types.SET_CURRENT_HABIT_EDIT] (state, habit) {
        state.habitEdit = habit ? habit.content : {}
        state.currentHabitIdEdit = habit ? habit.key : null
    },

    [types.SET_CURRENT_HABIT_EDIT_CLONE] (state, habit) {
        state.habitEditClone = habit ? habit : {}
    },

    [types.DELETE_HABIT_EDIT_CLONE_REMINDER] (state, hour) {
        let newObj = {}
        let keys = Object.keys(state.habitEditClone.remind.timeTriggers)
        keys.forEach(keyHour => {
            if (hour === keyHour)
                return false
            newObj[keyHour] = state.habitEditClone.remind.timeTriggers[keyHour]
        })
        
        state.habitEditClone.remind.timeTriggers = newObj
        Vue.set(state.habitEditClone.remind, 'timeTriggers', newObj)
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}