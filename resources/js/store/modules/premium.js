import * as types from '../mutation-types'
import { deviceTypes, customCountry, currencyPrefix, currencyPrefixISO } from '@/configs'
import moment from 'moment'

const state = {
    packages: [],
    currency: customCountry.us,
    history : {}
}

const getters = {
    packages: state => state.packages,
    currency: state => state.currency,
    currencyPrefix: state => currencyPrefix[state.currency],
    currencyPrefixISO: state => currencyPrefixISO[state.currency],
    history: state => state.history,
}

const actions = {
    setPackages ({ commit, state }, packages) {
        commit(types.SET_PACKAGES, packages)
    },
    setCurrenry ({ commit, state }, currenry) {
        commit(types.SET_CURRENCY, currenry)
    },
    setHistory ({ commit, state }, history) {
        commit(types.SET_HISTORY, history)
    },
}

const mutations = {
    [types.SET_PACKAGES] (state, packages) {
        state.packages = packages ? packages : []
    },
    [types.SET_CURRENCY] (state, currenry) {
        state.currenry = currenry ? currenry : customCountry.us
    },
    [types.SET_HISTORY] (state, history) {
        state.history = history ? history : {}
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}