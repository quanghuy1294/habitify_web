import * as types from '../mutation-types'
import { deviceTypes, habitStatus, upgradeKey } from '@/configs'
import moment from 'moment'
import Vue from 'vue'
import storage from 'store2'

const state = {
    deviceType: deviceTypes.pc,
    openNote: false,
    currentPathName: null,
    progress_habit_review: 'thisMonth',
    progress_daily_performance: 'last30day',
    progress_checkin_time: 'last30day',
    progress_completion_rate_type: true,
    progress_completion_rate_num: 4,

    detail_completion_rate_type: true,
    detail_completion_rate_num: 4,
    detail_weekday_status: habitStatus.done,

    manager_habit_type: 'active',

    sliderType: upgradeKey.habit,

    isShowPremium: false,

    stageMounted: true,

    isShowCompletedHabits: true,

    habitRefer: null,
}

const getters = {
    deviceType: state => state.deviceType,
    openNote: state => state.openNote,
    currentPathName: state => state.currentPathName,
    isMobile: state => parseInt(state.deviceType) !== parseInt(deviceTypes.pc),
    progress_habit_review: state => state.progress_habit_review,
    progress_daily_performance: state => state.progress_daily_performance,
    progress_checkin_time: state => state.progress_checkin_time,
    progress_completion_rate_type: state => state.progress_completion_rate_type,
    progress_completion_rate_num: state => state.progress_completion_rate_num,

    detail_completion_rate_type: state => state.detail_completion_rate_type,
    detail_completion_rate_num: state => state.detail_completion_rate_num,
    detail_weekday_status: state => state.detail_weekday_status,

    manager_habit_type: state => state.manager_habit_type,

    upgradeKey: state => state.upgradeKey,

    isShowPremium: state => state.isShowPremium,

    stageMounted: state => state.stageMounted,
    isShowCompletedHabits: state => state.isShowCompletedHabits,
    habitRefer: state => state.habitRefer,
}

const actions = {
    setDeviceType ({ commit, state }, type) {
        commit(types.SET_DEVICE_TYPE, type)
    },
    setOpenNote ({commit, state}, status) {
        commit(types.SET_OPEN_NOTE, status)
    },
    setCurrentPathName ({ commit, state }, name) {
        commit(types.SET_CURRENT_PATH_NAME, name)
    },
    setProgressHabitReviewType ({ commit, state }, type) {
        commit(types.SET_PREFERENCE_HABIT_REVIEW_TYPE, type)
    },
    setProgressDailyPerformanceType ({ commit, state }, type) {
        commit(types.SET_PREFERENCE_DAILY_PERFORMANCE_TYPE, type)
    },
    setProgressCheckinTimeType ({ commit, state }, type) {
        commit(types.SET_PREFERENCE_CHEKIN_TIME_TYPE, type)
    },
    setProgressComType ({ commit, state }, type) {
        commit(types.SET_PREFERENCE_COM_TYPE, type)
    },
    setProgressComNum ({ commit, state }, type) {
        commit(types.SET_PREFERENCE_COM_NUM, type)
    },

    setDetailComType ({ commit, state }, type) {
        commit(types.SET_PREFERENCE_DETAIL_COM_TYPE, type)
    },
    setDetailComNum ({ commit, state }, type) {
        commit(types.SET_PREFERENCE_DETAIL_COM_NUM, type)
    },
    setDetailWeekdayStatus ({ commit, state }, status) {
        commit(types.SET_PREFERENCE_DETAIL_WEEKDAY_STATUS, status)
    },

    setManagerHabitType ({ commit, state }, type) {
        commit(types.SET_PREFERENCE_MANAGER_HABIT_TYPE, type)
    },

    setUpgradeType ({ commit, state }, type) {
        commit(types.SET_PREFERENCE_UPGRADE_TYPE, type)
    },

    setPopupPremium ({ commit, state }, type) {
        commit(types.SET_POPUP_PREMIUM, type)
    },

    setStageMounted ({ commit, state }, stage) {
        commit(types.SET_STAGE_MOUNTED, stage)
    },

    setShowCompletedHabit ({ commit, state }, status) {
        commit(types.SET_SHOW_COMPLETED_HABIT, status)
    },

    setHabitDetailRefer ({ commit, state }, name) {
        commit(types.SET_HABIT_DETAIL_REFER, name)
    },
}

const mutations = {
    [types.SET_DEVICE_TYPE] (state, type) {
        Vue.set(state, 'deviceType', type)
    },
    [types.SET_OPEN_NOTE] (state, status) {
        Vue.set(state, 'openNote', status)
        storage.set('openNote', status)
    },
    [types.SET_CURRENT_PATH_NAME] (state, name) {
        storage.set('currentPage', name)
        Vue.set(state, 'currentPathName', name)
    },
    [types.SET_PREFERENCE_HABIT_REVIEW_TYPE] (state, type) {
        Vue.set(state, 'progress_habit_review', type)
        storage.set('progress_habit_review', type)
    },
    [types.SET_PREFERENCE_DAILY_PERFORMANCE_TYPE] (state, type) {
        Vue.set(state, 'progress_daily_performance', type)
        storage.set('progress_daily_performance', type)
    },
    [types.SET_PREFERENCE_CHEKIN_TIME_TYPE] (state, type) {
        Vue.set(state, 'progress_checkin_time', type)
        storage.set('progress_checkin_time', type)
    },
    [types.SET_PREFERENCE_COM_TYPE] (state, type) {
        Vue.set(state, 'progress_completion_rate_type', type)
        storage.set('progress_completion_rate_type', type)
    },
    [types.SET_PREFERENCE_COM_NUM] (state, type) {
        Vue.set(state, 'progress_completion_rate_num', type)
        storage.set('progress_completion_rate_num', type)
    },

    [types.SET_PREFERENCE_DETAIL_COM_TYPE] (state, type) {
        Vue.set(state, 'detail_completion_rate_type', type)
        storage.set('detail_completion_rate_type', type)
    },
    [types.SET_PREFERENCE_DETAIL_COM_NUM] (state, type) {
        Vue.set(state, 'detail_completion_rate_num', type)
        storage.set('detail_completion_rate_num', type)
    },

    [types.SET_PREFERENCE_DETAIL_WEEKDAY_STATUS] (state, type) {
        Vue.set(state, 'detail_weekday_status', type)
        storage.set('detail_weekday_status', type)
    },

    [types.SET_PREFERENCE_MANAGER_HABIT_TYPE] (state, type) {
        Vue.set(state, 'manager_habit_type', type)
        storage.set('manager_habit_type', type)
    },

    [types.SET_PREFERENCE_UPGRADE_TYPE] (state, type) {
        Vue.set(state, 'upgradeKey', type)
    },

    [types.SET_POPUP_PREMIUM] (state, status) {
        Vue.set(state, 'isShowPremium', status)
    },

    [types.SET_STAGE_MOUNTED] (state, stage) {
        Vue.set(state, 'stageMounted', stage)
    },

    [types.SET_SHOW_COMPLETED_HABIT] (state, status) {
        console.log('status from store: ', status)
        state.setShowCompletedHabit = status
        Vue.set(state, 'isShowCompletedHabits', status)
        storage.set('show_completed_habits', status)
    },

    [types.SET_HABIT_DETAIL_REFER] (state, name) {
        Vue.set(state, 'habitRefer', name)
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}