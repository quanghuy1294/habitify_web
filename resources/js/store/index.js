import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
// import * as getters from './getters'
import modules from './modules'
import mutations from './mutations'
import store from 'store2'

Vue.use(Vuex)

const state = {
    showMessage: true
}

export default new Vuex.Store({
    state,
    actions,
    // getters,
    mutations,
    modules,
    strict: false
})
