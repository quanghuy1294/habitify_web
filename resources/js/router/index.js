import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'

import Dashboard from '@/components/page/Dashboard'
import HabitDetail from '@/components/page/HabitDetail'
import Authenticate from '@/components/page/Authenticate'
import Signin from '@/components/page/Signin'
import Signup from '@/components/page/Signup'
import Forgot from '@/components/page/Forgot'
import Setting from '@/components/page/Setting'
import Progress from '@/components/page/Progress'
import ManagerHabit from '@/components/page/ManagerHabit'
import Swipe from '@/components/page/Swipe'

import Journal from '@/components/page/Journal'

Vue.use(Router)
Vue.use(Meta)

export default new Router({
    mode: 'history',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
        {
            path: '/journal/:time',
            name: 'Journal',
            component: Journal
        },
        {
            path: '/',
            name: 'Dashboard',
            component: Dashboard
        },
        {
            path: '/progress/:id/:edit?',
            // alias: '/progress/:id/edit',
            name: 'HabitDetail',
            component: HabitDetail
        },
        {
            path: '/auth',
            name: 'Authenticate',
            component: Authenticate
        },
        {
            path: '/signin',
            name: 'Signin',
            component: Signin
        },
        {
            path: '/signup',
            name: 'Signup',
            component: Signup
        },
        {
            path: '/forgot-password',
            name: 'Forgot',
            component: Forgot
        },
        {
            path: '/settings/:package?',
            name: 'Setting',
            component: Setting
        },
        {
            path: '/progress',
            name: 'Progress',
            component: Progress
        },
        {
            path: '/manage',
            name: 'ManagerHabit',
            component: ManagerHabit
        },
        {
            path: '/manage/:type',
            name: 'ManagerHabitType',
            component: ManagerHabit
        },
        {
            path: '/swipe',
            name: 'Swipe',
            component: Swipe
        },
        {
            path: '*',
            // component: NotFoundComponent
            redirect: { name: 'Dashboard' }
        }
    ]
})