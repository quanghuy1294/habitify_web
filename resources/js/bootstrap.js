
window._ = require('lodash')
// window.Paddle = require('@/plugins/paddle-sdk/paddlelib.js')

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
    // require('@/plugins/swipeSlide')
    require('@/plugins/weekdayChart')
    require('@/plugins/streakChart')
    require('chart.js')
    require('jquery-contextmenu/dist/jquery.contextMenu')
    require('jquery-contextmenu/dist/jquery.ui.position')
    // require('@/plugins/slideDropdown')
    require('hammerjs')
} catch (e) {}
