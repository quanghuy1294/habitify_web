<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    {{-- <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"> --}}
    <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Habitify Web</title>
    
    <link rel="preload" href="{{ mix('/css/app.css') }}" as="style">
    <link rel="preload" href="https://cdn.paddle.com/paddle/paddle.js" as="script">
    <link rel="preload" href="https://cdn.branch.io/branch-latest.min.js" as="script">
    <link rel="preload" href="/js/app.js" as="script">

    <link rel="image" href="/assets/images/icons/icon-192.png">
    <link rel="image" href="/assets/images/icons/icon-128.png">
    <link rel="image" href="/assets/images/icons/icon-128.png">
    <link rel="image" href="/assets/images/icons/icon-128.png">
    <link rel="image" href="/assets/images/icons/icon-16.png"/>

    <link rel="font" href="/fonts/icomoon.ttf"/>
    <link rel="font" href="/fonts/icomoon.eot"/>
    <link rel="font" href="/fonts/icomoon.woff"/>

    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="Habitify">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link rel="manifest" href="/manifest.json">

    <link rel="shortcut icon" sizes="192x192" href="/assets/images/icons/icon-192.png">
    <link rel="shortcut icon" sizes="128x128" href="/assets/images/icons/icon-128.png">
    <link rel="apple-touch-icon" sizes="128x128" href="/assets/images/icons/icon-128.png">
    <link rel="apple-touch-icon-precomposed" sizes="128x128" href="/assets/images/icons/icon-128.png">
    <link rel="shortcut icon" type="image/png" href="/assets/images/icons/icon-16.png"/>


<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '726620864417823');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=726620864417823&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-57KLX9X');</script>
<!-- End Google Tag Manager -->


</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-57KLX9X"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div id="app"></div>

    <script src="https://cdn.paddle.com/paddle/paddle.js"></script>
    <script src="https://cdn.branch.io/branch-latest.min.js"></script>
    <script defer src="{{ mix('/js/app.js') }}"></script>
    <script>
        branch.init('{{ env("MIX_BRANCH_KEY") }}');
    </script>
</body>
</html>
