const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
    resolve: {
        // extensions: ['.js', '.vue', '.json'],
        alias: {
            '@': __dirname + '/resources/js'
        },
    },
});

// mix.extract(['vue', 'jquery']);

mix.copyDirectory('resources/assets/js/plugins/chart.js', 'node_modules/chart.js');
mix.copyDirectory('resources/assets', 'public/assets');
mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.sass', 'public/css');


// mix.copyDirectory('resources/assets', 'html/assets');
// mix.copyDirectory('public/js', 'html/js')
//     .copyDirectory('public/css', 'html/css')
//     .copy('public/firebase-messaging-sw.js', 'html/firebase-messaging-sw.js');
