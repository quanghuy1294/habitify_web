<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

use Carbon\Carbon;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Illuminate\Support\Facades\File;


class PaddleController extends Controller
{
    protected $client;
    protected $vendorConfigs = [];

    /**
     * @var \Kreait\Firebase
     */
    protected $firebase;
    protected $auth;
    protected $database;

    public function __construct() {
        $this->client = new Client();

        $this->firebase = $this->firebaseAuth();
        $this->auth = $this->firebase->getAuth();
        $this->database = $this->firebase->getDatabase();

        $this->vendorConfigs = [
            'vendor_id' => env('MIX_PADDLE_VENDOR_ID'),
            'vendor_auth_code' => env('MIX_PADDLE_VENDOR_AUTH_CODE')
        ];
    }

    /**
     * @return \Kreait\Firebase
     */
    public function firebaseAuth()
    {
        //develop
        //$serviceAccount = ServiceAccount::fromJsonFile(public_path('habitify-development-firebase-adminsdk-si3de-fbd97140bd.json'));

        //product
        $serviceAccount = ServiceAccount::fromJsonFile(base_path(env('FIREBASE_KEY')));

        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            // The following line is optional if the project id in your credentials file
            // is identical to the subdomain of your Firebase project. If you need it,
            // make sure to replace the URL with the URL of your project.
            ->withDatabaseUri(env('FIREBASE_URI'))
            ->create();

        return $firebase;
    }

    public function getAllPackages(Request $request) {
        $locale = $request->get('currency', 'US');
        $products = $this->getListProduct();
        $subscription = $this->getListSubscription();

        $productIds = [];
        if (!empty($products->response) && !empty($products->response->count)) {
            foreach($products->response->products as $product) {
                $obj = new \stdClass();
                $obj->pdata = $product;
                $obj->ptype = 'product';
                $productIds[$product->id] = $obj;
            }
        }

        if (!empty($subscription->response)) {
            foreach($subscription->response as $product) {
                $obj = new \stdClass();
                $obj->pdata = $product;
                $obj->ptype = 'product';
                $productIds[$product->id] = $obj;
            }
        }

        $ids = array_keys($productIds);
        $productPrices = $this->getPriceOfProducts($ids, $locale);
        $resultProducts = [];
        if (!empty($productPrices->response) && !empty($productPrices->response->products)) {
            $resultProducts = $productPrices->response->products;
        }
        $collection = collect($resultProducts);
        $sorted = $collection->sortBy(function($product) {
            return $product->price->gross;
        });
        
        return response()->json($sorted->values()->all());
    }
    
    public function getListProduct() {
        $response = $this->client->post(
            'https://vendors.paddle.com/api/2.0/product/get_products',
            [
                'form_params' => $this->vendorConfigs
            ]
        );

        $body = $response->getBody();
        return json_decode((string) $body);
    }

    public function getListSubscription() {
        $response = $this->client->post(
            'https://vendors.paddle.com/api/2.0/subscription/plans',
            [
                'form_params' => $this->vendorConfigs
            ]
        );

        $body = $response->getBody();
        return json_decode((string) $body);
    }

    public function getPriceOfProducts ($products = [], $countryCode)  {
        $response = $this->client->get(
            'https://checkout.paddle.com/api/2.0/prices?product_ids=123456&customer_country=GB',
            [
                'query' => [
                    'product_ids' => implode(',', $products),
                    'customer_country' => $countryCode
                ]
            ]
        );

        $body = $response->getBody();
        return json_decode((string) $body);
    }

    public function getListTransactions () {

    }

    public function cancelSubscription (Request $request) {
        dd($request->all());
    }

    public function hookProcess(Request $request) {
        $filePath = public_path(('loghook.txt'));

        try {
            File::append($filePath, '____________' . Carbon::now()->format(Carbon::DEFAULT_TO_STRING_FORMAT) . '____________' . PHP_EOL);
            File::append($filePath, json_encode($request->all()));

            // Parse parram:
            $alertName = $request->get('alert_name');

            // process alert event
            if ($alertName == PAYMENT_REFUND || $alertName == SUBSCRIPTION_PAYMENT_REFUND) {
                $this->processRefund($request);
            } else if ($alertName == PAYMENT_SUCCESS || $alertName == SUBSCRIPTION_PAYMENT_SUCCESS) {
                $this->processPaySuccess($request);
            } else {
                File::append($filePath, 'Process unknow hook event' . PHP_EOL);
            }

            // write log user transaction
            $this->writeLogWeb($request);

            File::append($filePath, 'Complete with success status' . PHP_EOL);
            return response()->json(['status' => true]);
        } catch (\Exception $exception) {
            File::append($filePath, $exception->getMessage() . PHP_EOL);
            return response()->json(['status' => false], 500);
        }
    }

    public function processPaySuccess (Request $request) {
        $alertName = $request->get('alert_name');
        $expireDate = $request->get('next_bill_date');
        $uid = $request->get('passthrough');
        $updateInfo = [];

        if (!$uid)
            throw new \Exception('User not found', 400);

        if ($alertName == PAYMENT_SUCCESS) {
            $updateInfo['premiumStatus'] = IOS_PREMIUM;
            $updateInfo['premiumStatusAndroid'] = ANDROID_PREMIUM;
        } else {
            $updateInfo['premiumExpireDate'] = $expireDate .'T00:00:00+00:00';
        }

        foreach($updateInfo as $prop => $value) {
            $this->database->getReference("/users/$uid/$prop")->set($value);
        }
    }

    public function processRefund (Request $request) {

    }

    public function writeLogWeb(Request $request) {
        $uid = $request->get('passthrough', 'unknow');
        $time = time();
        $this->database->getReference("/logs/web/$uid/$time")->set($request->all());
    }

    public function switchSubscriptionPlan (Request $request) {

    }
}
