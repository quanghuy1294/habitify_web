<?php
    const PAYMENT_SUCCESS = 'payment_succeeded';
    const PAYMENT_REFUND = 'payment_refunded';
    const SUBSCRIPTION_PAYMENT_SUCCESS = 'subscription_payment_succeeded';
    const SUBSCRIPTION_PAYMENT_REFUND = 'subscription_payment_refunded';

    const IOS_FREEMIUM = 0;
    const IOS_PREMIUM = 9829;

    const ANDROID_FREMIUM = 0;
    const ANDROID_PREMIUM = 1;
?>